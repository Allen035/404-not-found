
CREATE DATABASE IF NOT EXISTS PortfolioManagement DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE PortfolioManagement;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for portfolio
-- ----------------------------
DROP TABLE IF EXISTS `portfolio`;
CREATE TABLE `portfolio` (
  `Notes` text COLLATE utf8_unicode_ci,
  `Cost-basis` double(15,0) DEFAULT NULL,
  `Shares` double(4,0) DEFAULT NULL,
  `No.of-lot` double(4,0) DEFAULT NULL,
  `Symbol` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ----------------------------
-- Table structure for Users
-- ----------------------------
CREATE TABLE IF NOT EXISTS `Users`(
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` varchar(50) DEFAULT NULL,
  `NAME` VARCHAR(20) DEFAULT NULL,
  `PSWD` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for watchlist
-- ----------------------------
DROP TABLE IF EXISTS `userportfolio`;
CREATE TABLE `userportfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `industry` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;




-- ----------------------------
-- Table structure for userwatchlist
-- ----------------------------
DROP TABLE IF EXISTS `userwatchlist`;
CREATE TABLE `userwatchlist` (
  `UserId` varchar(50) DEFAULT NULL,
  `watchlist` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for watchlistsymbols
-- ----------------------------

DROP TABLE IF EXISTS `watchlistsymbols`;
CREATE TABLE `watchlistsymbols` (
  `watchlist` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO watchlistsymbols VALUES('healthy-living','NKE');
INSERT INTO watchlistsymbols VALUES('healthy-living','LULU');
INSERT INTO watchlistsymbols VALUES('healthy-living','HLF');
INSERT INTO watchlistsymbols VALUES('healthy-living','UA');
INSERT INTO watchlistsymbols VALUES('healthy-living','DXCM');
INSERT INTO watchlistsymbols VALUES('healthy-living','FL');
INSERT INTO watchlistsymbols VALUES('healthy-living','DKS');
INSERT INTO watchlistsymbols VALUES('healthy-living','MED');
INSERT INTO watchlistsymbols VALUES('healthy-living','FIT');
INSERT INTO watchlistsymbols VALUES('healthy-living','NTRI');
INSERT INTO watchlistsymbols VALUES('healthy-living','GNC');

INSERT INTO watchlistsymbols VALUES('online-gaming-world','ATVI');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','EA');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','NTES');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','TTWO');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','CZR');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','MOMO');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','YY');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','IGT');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','CHDN');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','ZNGA');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','SOHO');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','CMCM');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','CYOU');
INSERT INTO watchlistsymbols VALUES('online-gaming-world','GLUU');

INSERT INTO watchlistsymbols VALUES('china-internet','BIDU');
INSERT INTO watchlistsymbols VALUES('china-internet','JD');
INSERT INTO watchlistsymbols VALUES('china-internet','NTES');
INSERT INTO watchlistsymbols VALUES('china-internet','WB');
INSERT INTO watchlistsymbols VALUES('china-internet','CTRP');
INSERT INTO watchlistsymbols VALUES('china-internet','WUBA');
INSERT INTO watchlistsymbols VALUES('china-internet','ATHM');
INSERT INTO watchlistsymbols VALUES('china-internet','VIPS');
INSERT INTO watchlistsymbols VALUES('china-internet','MOMO');
INSERT INTO watchlistsymbols VALUES('china-internet','SINA');
INSERT INTO watchlistsymbols VALUES('china-internet','YY');
INSERT INTO watchlistsymbols VALUES('china-internet','SOHO');
INSERT INTO watchlistsymbols VALUES('china-internet','CYOU');
INSERT INTO watchlistsymbols VALUES('china-internet','TOUR');


