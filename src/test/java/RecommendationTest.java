import com.portfolio.management.dao.StockDao;
import com.portfolio.management.dao.WatchListDao;
import com.portfolio.management.model.SymbolMetrics;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.portfolio.management.controller.RecommendationController;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class RecommendationTest {
    @Autowired
    StockDao stockDao;
    @Autowired
    WatchListDao watchListDao;
    @Autowired
    RecommendationController recommendationController;

    private static final Logger LOGGER = Logger.getLogger(RecommendationController.class);

    @Test
    public void recommendationTest(){
        RecommendationController rec = new RecommendationController();
        SymbolMetrics symbolMetrics = rec.getMetrics("FB");
        LOGGER.info(symbolMetrics.toString());
    }

    @Test
    public void test(){
        recommendationController.getRecommendations(Arrays.asList("MSFT","AAPL","AMZN","FB","A")).forEach(
                p -> LOGGER.info(p.getSimilarity())
        );
    }

}
