import com.portfolio.management.dao.StockDao;
import com.portfolio.management.model.RealTimeStock;
import com.portfolio.management.util.alphavantage.AlphavantageParserUtil;
import com.portfolio.management.util.alphavantage.AlphavantageUtil;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class AlphavantageParserUtilTest {

    @Autowired
    private StockDao stockDao;
    private static final Logger LOGGER = Logger.getLogger(AlphavantageParserUtilTest.class);

    private static final String SYMBOL = "MSFT";
    private static final int INTERVAL = 1;
    private static final String OUTPUT_SIZE = "compact";
    private static final String DATA_TYPE = "json";
    private static final String API_KEY = "X8RO597ZV071YDUA";
    private static final String SYMBOLS = "MSFT,FB,AAPL";

    @Test
    public void parseTimeSeriesIntraday(){
        AlphavantageParserUtil.parseTimeSeriesIntraday(SYMBOL,INTERVAL,OUTPUT_SIZE,DATA_TYPE,API_KEY).forEach(timeSeriesIntradayStock -> {
            LOGGER.info(timeSeriesIntradayStock);
        });
    }

    @Test
    public void parseTimeSeriesDaily(){
        AlphavantageParserUtil.parseTimeSeriesDaily(SYMBOL,OUTPUT_SIZE,DATA_TYPE,API_KEY).forEach(timeSeriesDailyStock -> {
            LOGGER.info(timeSeriesDailyStock.getLow());
        });

    }

    @Test
    public void parseTimeSeriesDailyAdjusted(){
        AlphavantageParserUtil.parseTimeSeriesDailyAdjusted(SYMBOL,OUTPUT_SIZE,DATA_TYPE,API_KEY).forEach(timeSeriesDailyAdjustedStock -> {
            LOGGER.info(timeSeriesDailyAdjustedStock);
        });
    }

    @Test
    public void parseTimeSeriesWeekly(){
        AlphavantageParserUtil.parseTimeSeriesWeekly(SYMBOL,DATA_TYPE,API_KEY).forEach(timeSeriesWeeklyStock -> {
            LOGGER.info(timeSeriesWeeklyStock);
        });
    }

    @Test
    public void parseTimeSeriesWeeklyAdjusted(){
        AlphavantageParserUtil.parseTimeSeriesWeeklyAdjusted(SYMBOL,DATA_TYPE,API_KEY).forEach(timeSeriesWeeklyAdjustedStock -> {
            LOGGER.info(timeSeriesWeeklyAdjustedStock);
        });
    }

    @Test
    public void parseTimeSeriesMonthly(){
        AlphavantageParserUtil.parseTimeSeriesMonthly(SYMBOL,DATA_TYPE,API_KEY).forEach(timeSeriesMonthlyStock -> {
            LOGGER.info(timeSeriesMonthlyStock);
        });
    }

    @Test
    public void parseTimeSeriesMonthlyAdjusted(){
        AlphavantageParserUtil.parseTimeSeriesMonthlyAdjusted(SYMBOL,DATA_TYPE,API_KEY).forEach(timeSeriesMonthlyAdjustedStock -> {
            LOGGER.info(timeSeriesMonthlyAdjustedStock);
        });
    }

    @Test
    public void parseBatchStockQuotes(){
        AlphavantageParserUtil.parseBatchStockQuotes(SYMBOLS,DATA_TYPE,API_KEY).forEach(stockQuote -> {
            LOGGER.info(stockQuote);
        });
    }



}
