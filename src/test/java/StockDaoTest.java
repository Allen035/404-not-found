import com.portfolio.management.dao.StockDao;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class StockDaoTest {
    @Autowired
    private StockDao stockDao;
    private static final Logger LOGGER = Logger.getLogger(WatchListDaoTest.class);


    @Test
    public void test(){
        stockDao.querySymbol("American","",0,10).forEach(
                p -> LOGGER.info(p)
        );
    }
}
