import com.portfolio.management.util.alphavantage.AlphavantageUtil;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class AlphavantageUtilTest {
    private static final Logger LOGGER = Logger.getLogger(AlphavantageUtilTest.class);
    private static final String SYMBOL = "MSFT";
    private static final String INTERVAL = "1min";
    private static final String OUTPUT_SIZE = "compact";
    private static final String DATA_TYPE = "json";
    private static final String API_KEY = "X8RO597ZV071YDUA";
    private static final String SYMBOLS = "MSFT,FB,AAPL";

    @Test
    public void getTimeSeriesIntraday(){
        LOGGER.info(AlphavantageUtil.getTimeSeriesIntraday(SYMBOL,INTERVAL,OUTPUT_SIZE,DATA_TYPE,API_KEY));

    }

    @Test
    public void getTimeSeriesDaily(){
        LOGGER.info(AlphavantageUtil.getTimeSeriesDaily(SYMBOL,OUTPUT_SIZE,DATA_TYPE,API_KEY));

    }

    @Test
    public void getTimeSeriesDailyAdjusted(){
        LOGGER.info(AlphavantageUtil.getTimeSeriesDailyAdjusted(SYMBOL,OUTPUT_SIZE,DATA_TYPE,API_KEY));
    }

    @Test
    public void getTimeSeriesWeekly(){
        LOGGER.info(AlphavantageUtil.getTimeSeriesWeekly(SYMBOL,DATA_TYPE,API_KEY));
    }

    @Test
    public void getTimeSeriesWeeklyAdjusted(){
        LOGGER.info(AlphavantageUtil.getTimeSeriesWeeklyAdjusted(SYMBOL,DATA_TYPE,API_KEY));
    }

    @Test
    public void getTimeSeriesMonthly(){
        LOGGER.info(AlphavantageUtil.getTimeSeriesMonthly(SYMBOL,DATA_TYPE,API_KEY));
    }

    @Test
    public void getTimeSeriesMonthlyAdjusted(){
        LOGGER.info(AlphavantageUtil.getTimeSeriesMonthlyAdjusted(SYMBOL,DATA_TYPE,API_KEY));
    }

    @Test
    public void getBatchStockQuotes(){
        LOGGER.info(AlphavantageUtil.getBatchStockQuotes(SYMBOLS,DATA_TYPE,API_KEY));
    }

}
