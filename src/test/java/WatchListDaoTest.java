import com.portfolio.management.controller.WatchlistController;
import com.portfolio.management.dao.WatchListDao;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class WatchListDaoTest {
    @Autowired
    private WatchListDao watchListDao;
    @Autowired
    private WatchlistController watchlistController;

    private static final Logger LOGGER = Logger.getLogger(WatchListDaoTest.class);
    private static final String userId = "z5131810@student.unsw.edu.au";

    @Test
    public void queryWatchSymbol(){

        watchListDao.queryWatchSymbol(userId,"","",0,1).forEach(
                LOGGER::info
        );
    }

    @Test
    public void testInsert(){
        watchListDao.addUserWatchlist(userId,"healthy-living");
    }

    @Test
    public void testQuery(){
        watchListDao.queryUserWatchList(userId).forEach(
                LOGGER::info
        );
    }

    @Test
    public void testHyphen(){
        LOGGER.info(watchlistController.convertFromHyphen("healthy-living"));
    }
}
