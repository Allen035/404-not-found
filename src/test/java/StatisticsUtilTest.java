import com.portfolio.management.util.StatisticsUtil;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.portfolio.management.controller.RecommendationController;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class StatisticsUtilTest {
    private static final Logger LOGGER = Logger.getLogger(StatisticsUtil.class);
    private static final String DATA_DIR = "training_data.csv";


    @Test
    public void test(){
        try{
            StatisticsUtil.readSymbolMetricsCSV(DATA_DIR).forEach(symbolMetrics ->
                    LOGGER.info(symbolMetrics.toString()));
        }
        catch (Exception e){
            LOGGER.info(e.getMessage());
        }
    }

    @Test
    public void testMode(){
        LOGGER.info(StatisticsUtil.getMode(Arrays.asList(1,2,3,4,5,1,2,3,0,4,3)));
    }
}
