<header>
    <div class="section">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Portfolio Management</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" id="header-search-text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="header-search-button">Search</button>
                </form>
            </div>
            <ul class="navbar-nav mr-auto">

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/portfolio">My Portfolio</a>
                        <a class="dropdown-item" href="/watchlist">My Watchlist</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" id="logout" href="#">Logout</a>
                    </div>
                </li>
                <li class="nav-item">
                    <img src="/resources/images/gitlogo.png" style="height:40px;" class="img-thumbnail"/>
                </li>
            </ul>
        </nav>
    </div>
    <script>
        function getCookie(name) {
            cookie = {};
            document.cookie.split(';').forEach(function (el) {
                [k, v] = el.split('=');
                cookie[k.trim()] = v;
            });
            return cookie[name];
        }

        function eraseCookie(name) {
            document.cookie = name + '=; Max-Age=-99999999;';
        }

        $('#navbarDropdownMenuLink').html(getCookie('USERNAME'));
        $('#logout').click(function () {
            eraseCookie('USERNAME');
            window.location.href = '/';
        });
        $('#header-search-button').click(function (e) {
            e.preventDefault();
            window.location.href = '/search?symbol='+$('#header-search-text').val();
        })


    </script>
</header>