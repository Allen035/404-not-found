<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Performance Analytics</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<div class="container-fluid">
    <div>
        <h3>Single Stock Analysis</h3>
        <table class="table-sm" style="width:100%;border:1px white solid">
            <thead>
            <tr bgcolor="#fff">
                <th scope="col" style="text-align: center">Symbol</th>
                <th scope="col" style="text-align: center">Company Name</th>
                <th scope="col" style="text-align: center">Last Price</th>
                <th scope="col" style="text-align: center">Change</th>
                <th scope="col" style="text-align: center">%Change</th>
                <th scope="col" style="text-align: center">Volume</th>
                <th scope="col" style="text-align: center">Short Term</th>
                <th scope="col" style="text-align: center">RSI</th>
            </tr>
            </thead>
            <tbody>
            <jstl:forEach items="${list}" var="list" varStatus="status">
                <tr bgcolor="${status.index%2 == 0?'#f5f5f7':'#fff'}">
                    <td align="center" style="color: #007bff; text-underline: #007bff"><a
                            href="quote?stockName=${list.symbol}">${list.symbol}</a></td>
                    <td align="center">${list.company}</td>
                    <td align="center">${list.price}</td>
                    <td align="center">${list.styledChange}</td>
                    <td align="center">${list.styledPercentageChange} %</td>
                    <td align="center">${list.volume}</td>
                    <td align="center">${list.shortTerm}</td>
                    <td align="center">${list.rsi}</td>
                </tr>
            </jstl:forEach>
            </tbody>
        </table>
        <h4>What is RSI?</h4>
        <p>The relative strength index (RSI) is a technical indicator used in the analysis of financial markets. It is intended to chart the current and historical strength or weakness of a stock or market based on the closing prices of a recent trading period.</p>
        <p>Traditionally, RSI readings greater than the 70 level are considered to be in overbought territory, and RSI readings lower than the 30 level are considered to be in oversold territory. In between the 30 and 70 level is considered neutral.</p>
        <a href="/recommendation">See my recommendations</a>
    </div>
    <div>
        <h3>Today's Market Analysis</h3>
        <p>The Dow retreat is the same. The focus on interest rates in the United States has been a topic of discussion for more than a year. Like a good horror movie the clues have been piling up, but markets like to pretend they have been caught by surprise.</p>

    </div>
</div>


</body>

</html>
