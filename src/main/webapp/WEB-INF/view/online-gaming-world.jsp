<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Online Gaming World</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<div class="container">
    <div id="carouselExampleIndicators" class="carousel slide col-lg-8 border" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img style="width:100%;height:380px"
                     src="https://s.yimg.com/dh/ap/finance/portfolio/online-gaming-world.jpg"
                     alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Online Gaming World</h5><button class="btn" id="watchlist-add">Add to My Watchlist</button>
                    <p>Given its convenience and social aspects, multiplayer online gaming could continue its fast growth and transform the gaming industry away from the console model.</p>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top:2rem">
        <em>Background</em>
        <p>The geeks shall inherit the earth. Unless they're too busy gaming. Multiplayer Online Role Playing Games (MMORPG), where players assume roles and do battle over the internet, are booming. It's social, it's cheap, and it's global. MMORPGs generate revenue from subscriptions and from selling virtual goods. PC gaming revenue rose 8% in 2012 to generate $20 billion worldwide, and it's expected to hit $25.7 billion by 2016. Meanwhile, US sales of game consoles fell 21% in 2012. The console market is in a two-year slump as consumers increasingly play Web-delivered games.

            MMORPGs are attractive business propositions: the development cost and time required are a fraction of those for console games and online games can scale rapidly.</p>
        <em>How did we choose these stocks?</em>
        <p>We identified US-listed stocks and American Depository Receipts of companies that are engaged in activities relevant to this watchlist's theme. We then filtered out companies that have a share price of less than $1.00 or a market capitalization less than $100 million, and excluded illiquid stocks by screening companies for liquidity i.e. average bid-ask spreads, dollar volume traded etc. Finally the proprietary Motif Optimization Engine determined the constituent stocks.</p>
        <em>Who made these selections?</em>
        <p>Motif is an online brokerage built on thematic portfolios of up to 30 stocks and ETFs. Founded in 2010 by Hardeep Walia, Motif combines complex proprietary algorithms with skilled advisers to develop these thematic portfolios.</p>
        <em>How are these weighted?</em>
        <p>First, we determined each company's percentage of total revenue derived from this watchlist's theme. Second, we applied a pure-play factor to give greater relative weight to companies that derive a higher percentage of their revenue from this theme. Finally, we weighted each company by its market capitalization adjusted for revenue exposure to the theme.</p>
    </div>
    <h3>Symbols</h3>
    <jsp:include page="symbollist.jsp"/>
</div>
<script>
    $('#watchlist-add').click(function () {
        $.post('/watchlist/add-watch',{'name':'online-gaming-world'});
    })
</script>
</body>

</html>
