<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Portfolio Management Login</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.min.css"/>
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="section">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Portfolio Management</h1>
                    <p class="lead">Login</p>
                    <a href="/">Home</a>
                </div>
            </div>
            <div>
                <form:form modelAttribute="login" action="login" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <form:input type="email" class="form-control" id="exampleInputEmail1" path="login"
                                    aria-describedby="emailHelp" placeholder="Enter email"/>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                            else.
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <form:input type="password" class="form-control" id="exampleInputPassword1" path="password"
                                    placeholder="Password"/>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <div>
                        <p style="color:red">${message}</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form:form>
            </div>
            <div>
                <a href="register">Register</a>
            </div>
        </div>
        <div class="col-4"></div>

    </div>

</div>

</body>
</html>
