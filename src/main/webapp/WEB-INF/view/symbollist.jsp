<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div>
    <table class="table-sm" style="width:100%;border:1px white solid">
        <thead>
        <tr bgcolor="#fff">
            <th scope="col" style="text-align: center">Symbol</th>
            <th scope="col" style="text-align: center">Company Name</th>
            <th scope="col" style="text-align: center">Last Price</th>
            <th scope="col" style="text-align: center">Change</th>
            <th scope="col" style="text-align: center">%Change</th>
            <th scope="col" style="text-align: center">Volume</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${list}" var="list" varStatus="status">
            <tr bgcolor="${status.index%2 == 0?'#f5f5f7':'#fff'}">
                <td align="center" style="color: #007bff; text-underline: #007bff"><a
                        href="/quote?stockName=${list.symbol}">${list.symbol}</a></td>
                <td align="center">${list.company}</td>
                <td align="center">${list.price}</td>
                <td align="center">${list.styledChange}</td>
                <td align="center">${list.styledPercentageChange}</td>
                <td align="center">${list.volume}</td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="page" id="paging"></div>
<script src="/resources/js/page.js"></script>
<script>
    $(function () {
        //pagination
        $("#paging").pagination({
            items: '${totalCount}',
            itemsOnPage: '${pageSize}',
            currentPage: '${pageNo}',
            cssStyle: 'light-theme'
        });
    });
</script>