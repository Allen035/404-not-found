<div class="section">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 10px">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/watchlist">Watchlist</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/portfolio">My Portfolio</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Markets
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/search">Stocks</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/recommendation">Recommendations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/performance-analytics">Performance Analytics</a>
                </li>
            </ul>
        </div>
    </nav>
</div>
