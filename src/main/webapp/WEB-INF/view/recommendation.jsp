<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Recommendation</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3>Spotlight Stocks</h3>
            <p>We have some handpicked stocks selected for you.</p>
            <div class="row">
                <div class="col-lg-2"></div>
                <div id="carouselExampleIndicators" class="carousel slide col-lg-8 border" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="img-thumbnail" style="width:100%;height:380px" src="/resources/images/MSFT.jpg"
                                 alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5><a href="/quote?stockName=MSFT">MSFT</a></h5>
                                <p>Profitable returns, low volatility</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="img-thumbnail" style="width:100%;height:380px" src="/resources/images/GOOGL.jpg"
                                 alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5><a href="/quote?stockName=GOOGL">GOOGL</a></h5>
                                <p>High price, reliable future</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="img-thumbnail" style="width:100%;height:380px" src="/resources/images/NVDA.jpg"
                                 alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5><a href="/quote?stockName=NVDA">NVDA</a></h5>
                                <p>Outstanding increase, widely watched</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <hr/>
            <h3>Recommendations</h3>
            <p>Here are some stocks from our recommendation system based on your taste.</p>
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">SYMBOL</th>
                    <th scope="col">COMPANY</th>
                    <th scope="col">SIMILARITY</th>
                </tr>
                </thead>
                <tbody>
                <jstl:forEach items="${recommendations}" var="recommendation" varStatus="status">
                    <tr>
                        <td><a href="quote?stockName=${recommendation.symbol}">${recommendation.symbol}</a></td>
                        <td>${companies[status.index]}</td>
                        <td>${recommendation.similarity}%</td>
                    </tr>
                </jstl:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
