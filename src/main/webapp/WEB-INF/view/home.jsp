<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Portfolio Management | Home</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>

<body>
<jsp:include page="header.jsp"/>
<div class="section">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Porfolio Management</h1>
            <p class="lead">Team 404 presents</p>
        </div>
    </div>
</div>
<jsp:include page="navbars.jsp"/>
<div class="container">
    <h2 style="padding-bottom: 1rem">Top Stories</h2>

    <div class="row">
        <div class="col-lg-12">
            <div id="news">

            </div>
        </div>
    </div>
</div>
<script>
    var FEED_URL = "https://finance.yahoo.com/rss/topstories";
    function getXML(theURL, callback)
    {
        /*
        * Original jQuery.ajax mid - CROSS DOMAIN AJAX
        */
        jQuery.ajax = (function(_ajax)
        {
            var protocol = location.protocol,
                hostname = location.hostname,
                exRegex = RegExp(protocol + '//' + hostname),
                YQL = 'http' + (/^https/.test(protocol)?'s':'') + '://query.yahooapis.com/v1/public/yql?callback=?',
                query = 'select * from html where url="{URL}" and xpath="*"';

            function isExternal(url)
            {
                return !exRegex.test(url) && /:\/\//.test(url);
            }

            return function(o)
            {
                var url = o.url;
                if (o.dataType == 'xml')   // @rickdog - fix for XML
                    query = 'select * from xml where url="{URL}"';	// XML
                if ( /get/i.test(o.type) && !/json/i.test(o.dataType) && isExternal(url) )
                {
                    // Manipulate options so that JSONP-x request is made to YQL
                    o.url = YQL;
                    o.dataType = 'json';
                    o.data = {
                        q: query.replace('{URL}', url + (o.data ? (/\?/.test(url) ? '&' : '?') + jQuery.param(o.data) : '')),
                        format: 'xml'
                    };

                    // Since it's a JSONP request
                    // complete === success
                    if (!o.success && o.complete) {
                        o.success = o.complete;
                        delete o.complete;
                    }

                    o.success = (function(_success)
                    {
                        return function(data)
                        {
                            if (_success) {
                                // Fake XHR callback.
                                _success.call(this, {
                                    // YQL screws with <script>s, Get rid of them
                                    responseText: (data.results[0] || '')
                                        .replace(/<script[^>]+?\/>|<script(.|\s)*?\/script>/gi, '')
                                }, 'success');
                            }
                        };
                    })(o.success);
                }
                return _ajax.apply(this, arguments); // not special, use base Jquery ajax
            };
        })(jQuery.ajax);


        return $.ajax({
            url: theURL,
            type: 'GET',
            dataType: 'xml',
            success: function(res) {
                // var text = res.responseText;
                // .. then you can manipulate your text as you wish
                callback ? callback(res) : undefined;
            }
        })
    }

    $(function() {
        getXML(FEED_URL,function (data) {
            $(data.responseText).find("item").each(function () { // or "item" or whatever suits your feed
                var el = $(this);
                var container = $('#news');
                container.append("<p style='margin-bottom:0rem;margin-top=1rem;'>" + el.find("source")[0].nextSibling.nodeValue +" • "+ el.find("pubDate").text()+"</p>");
                container.append("<h4>" + el.find("title").text() + "</h4>");
                container.append(el.find("description").text());
            });
        });
    });
</script>
</body>
</html>
