<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Portfolio</title>
    <link href="<c:url value="/resources/css/font-awesome.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>

    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>

    <style>
        body{
            background-color: #ffffff;
        }

        tbody.lots {
            display: none
        }

        .symbol_body {
            font-weight: bold;
        }

        .label {
            margin-left: 10px;
            margin-right: 5px;
            font-size: 18px;
        }

        .ln {
            font-size: 18px;
            font-weight: bold;
        }
    </style>
</head>

<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<div style="margin: 10px">
    <span id="sum_mv" style="font-size: xx-large;font-weight: bold">-</span>
    &nbsp;
    <%--<span>Update At:</span>--%>
    <%--<span id="update_at" class="label">-</span>--%>
    <span class="label">Day Gain:</span>
    <span id="sum_dg" class="ln ">-</span>
    &nbsp;
    <span class="label">Total Gain:</span>
    <span id="sum_tg" class="ln ">-</span>
    &nbsp;
    <span id="updating" style="color: #90b4c5;font-size: large"><i
            class="fa fa-spinner fa-spin fa-2x"></i>Updating...</span>


</div>
<c:forEach items="${symbols}" var="symbol" varStatus="status">
    <div style="width: 100%;">
        <table id="${symbol}" class="one" style="width:1200px;border: 0;margin-bottom: 30px" align="left"
               border="0"
               rules="none"
               cellspacing="1"
               cellpadding="1"
               align="right" align="center">
            <tbody class="symbol">
            <tr class="symbol_head" bgcolor="#eeeeee" style="color: #000000;" id="row_01">
                <th style="text-align: center" width="30px" height="40px"></th>
                <th style="text-align: left" width="80px"> Symbol</th>
                <th style="text-align: right" width="100px"> Shares</th>
                <th style="text-align: right" width="100px"> Cost Basis</th>
                <th style="text-align: right" width="150px">Market Value</th>
                <th style="text-align: right" width="150px">Day Gain</th>
                <th style="text-align: right" width="150px">Total Gain</th>
                <th style="text-align: left">&nbsp;&nbsp;&nbsp;&nbsp; Notes</th>
                <th style="text-align: center" width="100px"> Change</th>
                <th style="text-align: right" width="100px">No. of lots</th>
                <th style="text-align: center" width="30px">&nbsp;&nbsp;&nbsp;&nbsp; </th>
            </tr>


            <tr class="symbol_body">
                <td align="center" width="30px" height="40px"><input type="checkbox" id="checkbox_row_01"
                                                                     onChange="showOrHideLots(this)">
                </td>
                <td align="left"><a href="quote?stockName=${symbol}">${symbol}</a>
                    <div class="today_open_price" style="font-weight: bold">-</div>
                </td>
                <td class="shares" align="right">-</td>
                <td class="cb" align="right">-</td>
                <td class="mv" align="right">-</td>
                <td class="dg" align="right">-</td>
                <td class="tg" align="right">-</td>
                <td class="notes" align="left">&nbsp;&nbsp;&nbsp;&nbsp; -</td>
                <td class="change" align="right">-</td>
                <td class="nol" align="right">-</td>
                <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>

            </tbody>

            <tbody class="lots">
            <tr bgcolor="#eeeeee" style="color: #000000;" class="child_row_00">
                <th style="text-align: center" width="30px" height="30px"></th>
                <th style="text-align: left" width="80px">Lots</th>
                <th style="text-align: right" width="100px"> Shares</th>
                <th style="text-align: right" width="100px"> Cost Basis</th>
                <th style="text-align: right" width="150px">Market Value</th>
                <th style="text-align: right" width="150px">Day Gain</th>
                <th style="text-align: right" width="150px">Total Gain</th>
                <th style="text-align: left" colspan="3" width="410px">&nbsp;&nbsp;&nbsp;&nbsp; Notes</th>
                <th style="text-align: center" width="30px"></th>
            </tr>

            <tr style="color: #000000;display: none" class="default_lot_row">
                <td align="left" colspan="2">-</td>
                <td align="right"><input class="shares" type="number" name="Shares" style="width:90%; height:30px;"
                                         onblur="onLotRowChange(this)">
                </td>
                <td align="right"><input class="cb" type="number" name="CostBasis" style="width:90%; height:30px;"
                                         onblur="onLotRowChange(this)"></td>
                <td class="mv" align="right">-</td>
                <td class="dg" align="right">-</td>
                <td class="tg" align="right">-</td>
                <td align="center" colspan="3"><input class="notes" type="text" name="Notes"
                                                      style="width:90%; height:30px;"
                                                      onblur="onLotRowChange(this)"></td>
                <td align="left"><input type="button" value="Del" onClick="delALot(this)"></td>
            </tr>

            <c:forEach items="${lotss[status.index]}" var="lot" varStatus="innerStatus">
                <tr style="color: #000000;" class="lot_row" id="lot_${lot._id}">
                    <td align="left" colspan="2">-</td>
                    <td align="right"><input class="shares" type="number" name="Shares" style="width:90%; height:30px;"
                                             value="${lot.Shares}" onblur="onLotRowChange(this)">
                    </td>
                    <td align="right"><input class="cb" type="number" name="CostBasis" style="width:90%; height:30px;"
                                             value="${lot.get('Cost-basis')}" onblur="onLotRowChange(this)"></td>
                    <td class="mv" align="right">-</td>
                    <td class="dg" align="right">-</td>
                    <td class="tg" align="right">-</td>
                    <td align="center" colspan="3"><input class="notes" type="text" name="Notes"
                                                          style="width:90%; height:30px;"
                                                          value="${lot.Notes}"
                                                          onblur="onLotRowChange(this)"></td>
                    <td align="left"><input type="button" value="Del" onClick="delALot(this)"></td>
                </tr>
            </c:forEach>

            <tr class="add_lot" bgcolor="#eeeeee" style="color: #000000;">

                <td align="center"></td>
                <td align="left">
                    <button type="button" onClick="addALot(this)">Add lot</button>
                </td>
                <td align="center" colspan="9"></td>

            </tr>

            </tbody>


        </table>
    </div>
</c:forEach>

<script>

    /*
        基础变量、配置
     */
    var VARS = {
        BASE_URL: '${pageContext.request.contextPath}',
        API_URL: 'https://www.alphavantage.co/query',
        API_KEY: 'V8XAHB694RQWKB4F',
        NOTHING: '-'
    };

    /*
        变量缓存
     */
    var POOL = {
        SYMBOLS: JSON.parse('${symbolsJson}'),
        PRICE: {
            REAL_TIME_CLOSE: {},
            DAY_OPEN: {}
        },
        SUM: {
            DAY_GAIN: {},
            TOTAL_GAIN: {},
            MARKET_VALUE: {}
        },
        LAST_UPDATED: null,
        REQS: []
    };

    /*
        节点生产器
     */
    var PRODUCER = {
        text: {
            high: function (text) {
                return '<span style="color: green">' + text + '</span>'
            },
            low: function (text) {
                return '<span style="color: red">' + text + '</span>'
            },
            bold: function (text) {
                return '<span style="font-weight: bold">' + text + '</span>'
            },
            highOrLow4: function (num) {
                var me = PRODUCER.text;
                return !$.isNumeric(num) ? num
                    // 保留2位
                    : (num > 0 ? me.high(num.toFixed(2)) : me.low(num.toFixed(2)))
            },
            highOrLow: function (num) {
                var me = PRODUCER.text;
                return !$.isNumeric(num) ? num : (me[num > 0 ? 'high' : 'low'](num));
            }
        }
    }

    /**
     * 获取lot某一行的json数据
     * */
    function getLotRowData(ele) {
        var data = {};
        var symbol = $(ele).parents('table').attr('id');
        if (symbol) data['symbol'] = symbol;

        var lotTr;
        if ($(ele).is('tr.lot_row')) {
            lotTr = $(ele);
        } else {
            lotTr = $(ele).parents('tr.lot_row');
        }
        if (lotTr.length == 0) return data;
        var id = lotTr.attr('id');
        if (id) {
            data['id'] = id.substring(4);
        }
        var shares = lotTr.find('input.shares').val();
        if (shares) data['shares'] = shares;
        var cb = lotTr.find('input.cb').val();
        if (cb) data['cost_Basis'] = cb;
        var notes = lotTr.find('input.notes').val();
        if (notes) data['notes'] = notes;
        return data;
    }

    // 监听lot行里面的文本框的输入改变事件
    function onLotRowChange(input) {
        // 传给后台，编辑修改这条lot
        // 计算需要计算的数据？
        var data = getLotRowData(input);
        if (// 有效数据
        data.cost_Basis || data.shares || data.notes
        ) {
            if (data.id) {
                // update
                $.post(VARS.BASE_URL + '/portfolio/lots/update', data, function (resCode, status, jqXHR) {
                    // if (resCode && resCode > 0)
                    console.log(resCode);
                    refreshAllData();
                });
            } else {
                // insert
                $.post(VARS.BASE_URL + '/portfolio/lots/add', data, function (resId, status, jqXHR) {
                    if (resId && resId > 0) {
                        $(input).parents('tr.lot_row').attr('id', 'lot_' + resId);
                    }
                    refreshAllData();
                });
            }
        }

    }

    // 点击股票的复选框，显示或者隐藏子lots
    function showOrHideLots(chk) {
        var it = $(chk);
        it.parents('table.one').find('tbody.lots')[it.prop('checked') === true ? 'show' : 'hide']();
    }

    /*
        add lot
     */
    function addALot(btn) {
        var it = $(btn);
        var aCopy = $('tr.default_lot_row').first().clone().removeClass('default_lot_row').addClass('lot_row');
        aCopy.insertBefore(it.parents('tr.add_lot'));
        aCopy.show();
        /*// 传给后台，添加
        $.post('', {}, function (resJson, status, jqXHR) {
            if (resJson) {

            }
        })*/
    }


    /*
        del lot
     */
    function delALot(btn) {
        var it = $(btn);
        var data = getLotRowData(btn);
        if (data.id) {
            // delete
            $.post(VARS.BASE_URL + '/portfolio/lots/delete', data, function (resCode, status, jqXHR) {
                console.log(resCode);
                if (resCode && resCode > 0) {
                    it.parents('tr').remove();
                    refreshAllData();
                } else {
                    alert('delete failed！')
                }
            });
        } else {
            it.parents('tr').remove();
            refreshAllData();
        }
    }

    /*
        请求实时价格 BATCH_STOCK_QUOTES
     */
    function reqRealTimePrice(callback) {
        $('#updating').show();
        var req = $.getJSON(VARS.API_URL, {
            'function': 'BATCH_STOCK_QUOTES',
            symbols: POOL.SYMBOLS.join(","),
            apikey: VARS.API_KEY
        }, function (resJson, status, jqXHR) {
            if (resJson) {
                var arr = resJson['Stock Quotes'];
                $.each(arr, function (i, one) {
                    POOL.PRICE.REAL_TIME_CLOSE[one['1. symbol']] = one['2. price'];
                    if (i === arr.length - 1) {
                        // 简单记录一下更新时间
                        POOL.LAST_UPDATED = one['4. timestamp']
                    }
                });
                if ($.isFunction(callback)) callback();
                console.log(JSON.stringify(POOL.PRICE.REAL_TIME_CLOSE))
            }
            POOL.REQS.splice(POOL.REQS.indexOf(req), 1);
            if (POOL.REQS.length === 0) {
                $('#updating').hide()
            }
        });
        POOL.REQS.push(req);
    }

    /*
        请求当日开盘价 TIME_SERIES_DAILY
     */
    function reqTodayOpenPrice(callback) {
        $('#updating').show();
        $.each(POOL.SYMBOLS, function (i, symbol) {
            var req = $.getJSON(VARS.API_URL, {
                'function': 'TIME_SERIES_DAILY',
                symbol: symbol,
                apikey: VARS.API_KEY
            }, function (resJson, status, jqXHR) {
                if (resJson) {
                    var many = resJson['Time Series (Daily)'];
                    if (many)
                        for (var date in many) {
                            var one = many[date];
                            POOL.PRICE.DAY_OPEN[symbol] = one['1. open'];
                            break;
                        }
                    if ($.isFunction(callback)) callback();
                    console.log(JSON.stringify(POOL.PRICE.DAY_OPEN))
                }
                // POOL.REQS.splice(POOL.REQS.indexOf(req), 1);
                POOL.REQS.splice(POOL.REQS.indexOf(req), 1);
                if (POOL.REQS.length === 0) {
                    $('#updating').hide()
                }
            })
            POOL.REQS.push(req);
        })
    }


    /*
        请求获取每个股票的Lots
     */
    /*function reqLots() {
        $.each(POOL.SYMBOLS, function (i, symbol) {
            $.post(VARS.BASE_URL + '/portfolio/lots', {symbol: symbol}, function (resJson, status, jqXHR) {
                if (resJson) {
                    console.log(resJson);
                }
            })
        });
    }*/

    // 定时刷新数据，5s刷新一次
    window.setInterval(function () {
        reqRealTimePrice(refreshAllData);
        reqTodayOpenPrice(refreshAllData);
    }, 5000);

    reqRealTimePrice(refreshAllData);
    reqTodayOpenPrice(refreshAllData);

    /*
        计算刷新所有数据
     */
    function refreshAllData() {
        POOL.SUM.DAY_GAIN = {}
        POOL.SUM.TOTAL_GAIN = {}
        POOL.SUM.MARKET_VALUE = {}
        var num4 = PRODUCER.text.highOrLow4,
            num = PRODUCER.text.highOrLow,
            NOTHING = VARS.NOTHING;
        $.each(POOL.SYMBOLS, function (i, aSymbol) {
            var table = $('#' + aSymbol);
            var todayOpenPrice = POOL.PRICE.DAY_OPEN[aSymbol],
                realTimePrice = POOL.PRICE.REAL_TIME_CLOSE[aSymbol],
                pt = table.find('tbody.symbol'),
                ct = table.find('tbody.lots');
            // 设置当日开盘价
            pt.find('.today_open_price').html(todayOpenPrice ? todayOpenPrice : NOTHING);
            // 设置change
            pt.find('td.change').html(realTimePrice && todayOpenPrice ? num4(realTimePrice - todayOpenPrice) : NOTHING);

            // 开始遍历lots
            var rows = ct.find('tr.lot_row'),
                length = rows.length;
            var sum = {};
            // 有子lot的
            if (length > 0) {
                rows.each(function (i, tr) {
                    var shares = $(tr).find('input.shares').val();
                    var basis = $(tr).find('input.cb').val();
                    // market value
                    var tdmv = $(tr).find('td.mv').html(realTimePrice && shares ? num4(realTimePrice * shares) : NOTHING);
                    // day gain
                    var tddg = $(tr).find('td.dg').html(realTimePrice && todayOpenPrice && shares ? num4((realTimePrice - todayOpenPrice) * shares) : NOTHING);
                    // total gain
                    var tdtg = $(tr).find('td.tg').html(realTimePrice && basis && shares ? num4((realTimePrice - basis) * shares) : NOTHING);
                    // 下面求和
                    // shares
                    if (shares && $.isNumeric(shares)) sum['shares'] = sum['shares'] ? (sum['shares'] + Number(shares)) : Number(shares)
                    // cost basis →basis * shares  √
                    if (basis && $.isNumeric(basis)) sum['cb'] = sum['cb'] ? (sum['cb'] + Number(basis)*Number(shares)) : Number(basis)*Number(shares)
                    // market value
                    if (realTimePrice && shares) {
                        var mv = Number(tdmv.text());
                        sum['mv'] = sum['mv'] ? (sum['mv'] + mv) : mv
                    }
                    // day gain
                    if (realTimePrice && todayOpenPrice && shares) {
                        var dg = Number(tddg.text());
                        sum['dg'] = sum['dg'] ? (sum['dg'] + dg) : dg
                    }
                    // total gain
                    if (realTimePrice && basis && shares) {
                        var tg = Number(tdtg.text());
                        sum['tg'] = sum['tg'] ? (sum['tg'] + tg) : tg
                    }
                });
                pt.find('td.nol').html(length);
            } else {
                pt.find('td.nol').html('0');
            }
            // 设置父行的数据
            ['shares', 'cb', 'mv', 'dg', 'tg'].forEach(function (it) {
                pt.find('td.' + it).html(sum[it] ? ('shares' === it ? num(sum[it]) : num4(sum[it])) : NOTHING)
            })
            if (sum['dg']) POOL.SUM.DAY_GAIN[aSymbol] = sum['dg']
            if (sum['tg']) POOL.SUM.TOTAL_GAIN[aSymbol] = sum['tg']
            if (sum['mv']) POOL.SUM.MARKET_VALUE[aSymbol] = sum['mv']
        });
        // 设置最上面
        var s = 0;
        $.each(POOL.SUM.DAY_GAIN, function (symbol, val) {
            s += val;
        });
        if ($.isNumeric(s)) $('#sum_dg').html(num4(s));
        s = 0;
        $.each(POOL.SUM.TOTAL_GAIN, function (symbol, val) {
            s += val;
        });
        if ($.isNumeric(s)) $('#sum_tg').html(num4(s));
        s = 0;
        $.each(POOL.SUM.MARKET_VALUE, function (symbol, val) {
            s += val;
        });
        if ($.isNumeric(s)) $('#sum_mv').html($(num4(s)).css({'color': 'black'}));
        // 简单的记录更新时间
        if (POOL.LAST_UPDATED) $('#update_at').html(POOL.LAST_UPDATED)
    }
</script>


</body>
</html>