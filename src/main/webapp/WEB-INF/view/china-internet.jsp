<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>China Internet</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<div class="container">
    <div id="carouselExampleIndicators" class="carousel slide col-lg-8 border" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img style="width:100%;height:380px"
                     src="https://s.yimg.com/dh/ap/finance/portfolio/china-internet.jpg"
                     alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>China Internet</h5><button class="btn" id="watchlist-add">Add to My Watchlist</button>
                    <p>Chinese internet companies grow their users bases and their ability to monetize them, could result in rapid revenue and earnings growth.</p>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top:2rem">
        <em>Background</em>
        <p>China's already the biggest internet market in the world. But their 591 million users represent a tiny 44% penetration rate. Which means there's plenty of room to grow. During the first half of 2013, 26 million Chinese opened a browser for the first time. <br><br>But here’s the angle: China's internet market is dominated by local companies. They'll skip over Google to search with Baidu, which controls 65.7% of the market. China's online advertising market reached $11 billion in 2012, up 46.8% over 2011 and is expected to grow further. China has already surpassed the U.S. as the world's largest market for smartphones and by the end of 2013 it is expected to be twice the size of the U.S. market. All of which has many investors looking East for potential good fortune.</p>
        <em>How did we choose these stocks?</em>
        <p>We identified US-listed stocks and American Depository Receipts of companies that are engaged in activities relevant to this watchlist's theme. We then filtered out companies that have a share price of less than $1.00 or a market capitalization less than $100 million, and excluded illiquid stocks by screening companies for liquidity i.e. average bid-ask spreads, dollar volume traded etc. Finally the proprietary Motif Optimization Engine determined the constituent stocks. <a href="https://trader.motifinvesting.com/view/motif-construction-process" data-rapid_p="49">Learn more</a> about how we select our watchlists.</p>
        <em>Who made these selections?</em>
        <p>Motif is an online brokerage built on thematic portfolios of up to 30 stocks and ETFs. Founded in 2010 by Hardeep Walia, Motif combines complex proprietary algorithms with skilled advisers to develop these thematic portfolios. <a href="https://www.motifinvesting.com/about/leadership-team" data-rapid_p="50">Learn more</a> about our team.</p>
        <em>How are these weighted?</em>
        <p>First, we determined each company's percentage of total revenue derived from this watchlist's theme. Second, we applied a pure-play factor to give greater relative weight to companies that derive a higher percentage of their revenue from this theme. Finally, we weighted each company by its market capitalization adjusted for revenue exposure to the theme.</p>
    </div>
    <h3>Symbols</h3>
    <jsp:include page="symbollist.jsp"/>
</div>
<script>
    $('#watchlist-add').click(function () {
        $.post('/watchlist/add-watch',{'name':'china-internet'});
    })
</script>
</body>

</html>
