<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Portfolio Management Show</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<div class="container">
    <div>
        <h3>Watchlists</h3>
        <div class="row">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://s.yimg.com/dh/ap/finance/portfolio/healthy-living.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Healthy Living</h5>
                    <p class="card-text">This basket consists of stocks gaining popularity from health and wellness.</p>
                    <a href="watchlist/detail?w=healthy-living" class="btn btn-primary">Check it out</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://s.yimg.com/dh/ap/finance/portfolio/china-internet.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">China Internet</h5>
                    <p class="card-text">Chinese internet companies grow their users bases and their ability to monetize them, could result in rapid revenue and earnings growth.</p>
                    <a href="watchlist/detail?w=china-internet" class="btn btn-primary">Check it out</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://s.yimg.com/dh/ap/finance/portfolio/online-gaming-world.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Online Gaming World</h5>
                    <p class="card-text">Given its convenience and social aspects, multiplayer online gaming could continue its fast growth and transform the gaming industry away from the console model.</p>
                    <a href="watchlist/detail?w=online-gaming-world" class="btn btn-primary">Check it out</a>
                </div>
            </div>
        </div>
        <h3 style="margin-top:2rem">My watchlist</h3>
        <table class="table table-responsive-sm">
            <thead>
            <tr>
                <th scope="col">Watchlist</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${watchlists}" var="watchlist">
                <tr>
                    <td><a href="watchlist/detail?w=${watchlist.hyphenName}">${watchlist.name}</a></td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </div>
</div>
</body>
</html>