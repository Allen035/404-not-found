<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Portfolio Management</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>

</head>
<body>

<div class="section">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Porfolio Management</h1>
            <p class="lead">Team 404 presents</p>
            <p class="lead">
                <a href="/login">Login</a>
                <span> | </span>
                <a href="/register">Register</a>
                <span> | </span>
                <a href="/home">Home</a>
            </p>
        </div>
    </div>
</div>
<%--<div data-spy="scroll" data-target="#navbar-example2" data-offset="0">--%>
    <%--<div class="container-fluid" id="intro" style="padding-top:50px;height:500px;background-image:url('/resources/images/index.jpg');background-blend-mode: lighten;">--%>
        <%--<h1 style="text-align: center;"><b>INTRODUCING</b></h1>--%>
        <%--<h1 style="text-align: center;"><b>Portfolio Management</b></h1>--%>
        <%--<p style="text-align: center;">Team 404 presents</p>--%>
    <%--</div>--%>
    <%--<div class="container-fluid" id="portfolio" style="padding-top:50px;height:500px;color: #fff; background-color: #1E88E5;">--%>
        <%--<h4>Portfolio Management</h4>--%>
        <%--<p>Hand your portfolio to us and manage you everyday earnings efficiently.</p>--%>
    <%--</div>--%>
    <%--<div class="container-fluid" id="watchlist" style="padding-top:50px;height:500px;color: #fff; background-color: #673ab7;">--%>
        <%--<h4>Watchlists</h4>--%>
        <%--<p>Follow our favourite pre-generated stock list to get started and get inspired.</p>--%>
    <%--</div>--%>
    <%--<div class="container-fluid" id="recommendation" style="padding-top:50px;height:500px;color: #fff; background-color: #00bcd4;">--%>
        <%--<h4>Recommendation</h4>--%>
        <%--<p>Get our expert recommendations based on your taste.</p>--%>
    <%--</div>--%>
    <%--<div class="container-fluid" id="performance-analytics" style="padding-top:50px;height:500px;color: #fff; background-color: #009688;">--%>
        <%--<h4>Performance Analytics</h4>--%>
        <%--<p>Look at your portfolio closely.</p>--%>
    <%--</div>--%>
<%--</div>--%>

</body>
</html>
