<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Quote</title>
	<link href="<c:url value="/resources/css/bootstrap.min.css" />"
		  rel="stylesheet">
	<link href="<c:url value="/resources/css/stock.css" />" rel="stylesheet">
	<script src="/resources/js/jquery-3.3.1.min.js"></script>
	<script src="/resources/js/bootstrap.bundle.min.js"></script>
	<script src="https://code.highcharts.com/stock/highstock.js"></script>
	<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/stock/modules/export-data.js"></script>

</head>
<body>
<jsp:include page="header.jsp" />
<jsp:include page="navbars.jsp" />

<div class="all_style">
    <div>
	    <h4>
		    <span>${stockLongName}</span>
		    &nbsp;
		    <span>(^${stockName})</span>
	    </h4>
    </div>

    <div>
        <p style="font-size:15px">Real Time Price</p>
    </div>

    <div>
	    <span style="font-size:55px" id="yOpen">-</span>
	    &nbsp;
	    <span style="font-size:38px" id="diff">-</span>
	    &nbsp;
	    <span style="font-size:38px" id="sign_1">(</span>
	    <span style="font-size:38px" id="change">-</span>
	    <span style="font-size:38px" id="sign_2"> %)</span>
    </div>

    <div>
	    <button type="button" class="add_to_watchlist" onclick="addOrRemove()"
			id="add-to-watchlist">Add to watchlist</button>
    </div>


    <div class="quote-send">
	    <div class="input-group mb-3">
		    <input type="text" id="search-stock-field" class="form-control" placeholder="quote lookup">
		    <button id="search-btn">click me</button>
	    </div>
    </div>

    <hr class="line_1_style" id="line_1">

    <div>
    <table id="stock_details">
	    <tbody>
	    <tr>
		    <td id="summary-cell">
			   <table id="summary-block" class="table">
				    <tbody>
				    <tr style="height: 100%">
					    <td>Previous Open</td>
					    <th id="open_price" scope="row"></th>
					    <td>Previous Close</td>
					    <th id="close_price" scope="row"></th>
				    </tr>
				    <tr>
					    <td>Previous High</td>
					    <th id="high_price" scope="row"></th>
					    <td>Previous Low</td>
					    <th id="low_price" scope="row"></th>
				    </tr>
				    <tr>
					    <td>adjusted close</td>
					    <th id="adjusted_close_price" scope="row"></th>
                        <td>Volume</td>
                        <th id="volume" scope="row"></th>
				    </tr>
                    <tr>
                        <td>dividend amount</td>
                        <th id="dividend_amount" scope="row"></th>
                        <td>split coefficient</td>
                        <th id="split_coefficient" scope="row"></th>
                    </tr>
				    </tbody>
			   </table>
		    </td>
            <td id="container" style="height: 430px; width: 900px">
		    </td>
	    </tr>
	    </tbody>
    </table>
    </div>


    <hr class="line_2_style" id="line_2">

    <div class="information_board">
        <div class="information_board_small">
            <div style="float:left">
                <a href="https://finance.yahoo.com/news/did-altaba-inc-nasdaq-aaba-171531138.html" title="This is image linking">
                    <img alt="imageone" src="/resources/images/image_1.jpg" width="210" height="120" />
                </a>
            </div>
            <div style="overflow:auto">
               <a class="link_title" href="https://finance.yahoo.com/news/did-altaba-inc-nasdaq-aaba-171531138.html">
                How Did Altaba Inc’s (NASDAQ:AABA) 35.43% ROE Fare Against The Industry?
               </a>
               <div>
                   <a class="link_content" href="https://finance.yahoo.com/news/did-altaba-inc-nasdaq-aaba-171531138.html">
                    With an ROE of 35.43%, Altaba Inc (NASDAQ:AABA) outpaced its own industry which
                   delivered a less exciting 13.50% over the past year. On the surface,
                       this looks fantastic since we know that AABA has made large...
                   </a>
               </div>
            </div>
            <p class="like" id="heart_show_1">♡<span style="font-size:20px">like</span></p>
        </div>
        <p></p>
        <div class="information_board_small">
            <div style="float:left">
                <a href="https://finance.yahoo.com/news/did-altaba-inc-nasdaq-aaba-171531138.html" title="This is image linking">
                    <img alt="imagetwo" src="/resources/images/image_2.jpg" width="210" height="120" />
                </a>
            </div>
            <div style="overflow:auto">
                <a class="link_title" href="https://finance.yahoo.com/news/cryptocurrencies-track-hefty-monthly-gains-155800684.html">
                  Cryptocurrencies on Track for Hefty Monthly Gains; Bitcoin Lags the Pack
                </a>
                <div>
                    <a class="link_content" href="https://finance.yahoo.com/news/cryptocurrencies-track-hefty-monthly-gains-155800684.html">
                        Investing.com - Cryptocurrencies were on track for heavy monthly gains on Monday, though Bitcoin,
                        the number one digital currency by market capitalization lagged the pack.
                    </a>
                </div>
            </div>
            <p style="overflow:auto" class="like" id="heart_show_2">♡<span style="font-size:20px">like</span></p>
        </div>
        <p></p>
        <div class="information_board_small">
            <div style="float:left">
                <a href="https://finance.yahoo.com/m/4ac164f3-b672-31a2-8472-07528adcb07d/want-fangs-with-drips%3F-check.html"
                   title="This is image linking">
                    <img alt="imagethree" src="/resources/images/image_3.jpg" width="210" height="120" />
                </a>
            </div>
            <div style="overflow:auto">
                <a class="link_title" href="https://finance.yahoo.com/m/4ac164f3-b672-31a2-8472-07528adcb07d/want-fangs-with-drips%3F-check.html">
                Want FANGs with DRIPs? Check Out the 'CIMBA' Stocks
                </a>
                <div>
                    <a class="link_content" href="https://finance.yahoo.com/m/4ac164f3-b672-31a2-8472-07528adcb07d/want-fangs-with-drips%3F-check.html">
                        Despite recent weakness, the technology sector has been among the best-performing groups for well over a year.
                        -- observes Chuck Carlson, editor of DRIP Investor and a contributor to MoneyShow.com. The one...
                    </a>
                </div>
            </div>
            <p style="overflow:auto" class="like" id="heart_show_3">♡<span style="font-size:20px">like</span></p>
        </div>
        <p></p>
        <div class="information_board_small">
            <div style="float:left">
                <a href="https://finance.yahoo.com/news/bitcoin-shrugs-off-buffett-criticism-230800446.html"
                   title="This is image linking">
                    <img alt="imagefour" src="/resources/images/image_4.jpg" width="210" height="120" />
                </a>
            </div>
            <div style="overflow:auto">
                <a class="link_title" href="https://finance.yahoo.com/news/bitcoin-shrugs-off-buffett-criticism-230800446.html">
                 Bitcoin Shrugs Off Buffett Criticism to Remain Above $9,000
                </a>
                <div>
                    <a class="link_content" href="https://finance.yahoo.com/news/bitcoin-shrugs-off-buffett-criticism-230800446.html">
                        Investing.com – Bitcoin traded roughly unchanged on Monday, holding above $9,000 supported by a recent
                        uptick in sentiment on cryptocurrencies, while EOS maintained its position as the fifth most valuable...
                    </a>
                </div>
            </div>
            <p style="overflow:auto" class="like" id="heart_show_4">♡<span style="font-size:20px">like</span></p>
        </div>
    </div>
</div>
<script>



    /*
        get API
    */
    var VARS = {
        BASE_URL: '${pageContext.request.contextPath}',
        API_URL: 'https://www.alphavantage.co/query',
        API_KEY: 'V8XAHB694RQWKB4F',
        NOTHING: '-'
    };


    function reqPrice(callback) {

        var req = $.getJSON(
            VARS.API_URL,
            {
                'function': 'TIME_SERIES_DAILY_ADJUSTED',
                symbol: "${stockName}",
                apikey: VARS.API_KEY
            },
            function (resJson, status, jqXHR) {
                if (resJson) {
                    var many = resJson['Time Series (Daily)'];
                    var count = 0
					var todayOpen, yesterdayOpen
                    if (many)
                        for (var date in many) {

                            var one = many[date];
                            if(count == 0) {
                                $("#open_price").text(one['1. open']);
                                $("#close_price").text(one['4. close']);
                                $("#high_price").text(one['2. high']);
                                $("#low_price").text(one['3. low']);
                                $("#adjusted_close_price").text(one['5. adjusted close']);
                                $("#volume").text(one['6. volume']);
                                $("#dividend_amount").text(one['7. dividend amount']);
                                $("#split_coefficient").text(one['8. split coefficient']);
                                todayOpen = one['1. open']
                                count = count + 1
							} else {
                                yesterdayOpen = one['1. open']
								break;
                            }
                        }
					$("#yOpen").text(parseFloat(yesterdayOpen).toFixed(2))
					var diff = todayOpen - yesterdayOpen
					$("#diff").text((diff > 0 ? "+" : "") + parseFloat(diff).toFixed(2))
                    color_change()
					$("#change").text((diff > 0 ? "+" : "") + parseFloat(diff/yesterdayOpen).toFixed(2))


                }
            })
    }


       /*
          color change
        */
     function color_change(){
         var diff_value_div=document.getElementById("diff");
         var change_value_div=document.getElementById("change");
         var sign_1_div=document.getElementById("sign_1");
         var sign_2_div=document.getElementById("sign_2");
         var line_1_div=document.getElementById("line_1");
         var line_2_div=document.getElementById("line_2");
         var diff_value=diff_value_div.innerHTML;
         var col=Number(diff_value)>"0"?"green":"red";
         diff_value_div.style.color=col;
         change_value_div.style.color=col;
         sign_1_div.style.color=col;
         sign_2_div.style.color=col;
         line_1_div.style.background=col;
         line_2_div.style.background=col;
     }

     /*
         add or remove from list
      */
    function addOrRemove() {
        var data = {};
        $.post("/watchlist/check",
            {"symbol": "${stockName}"},
            function(result){
                data.isInWatchList = (result == "true" ? true : false)

                if(!data.isInWatchList) {
                    // add
                    $.post("/watchlist/add",
                        {"symbol": "${stockName}"},
                        function(result){
                            alert("Do you want to add ${stockName} to watchlist?")
                            // update button label
                            updateWatchListButtonLabel();
                        });
                } else {
                    // remove
                    $.post("/watchlist/remove",
                        {"symbol": "${stockName}"},
                        function(result){
                            alert("Do you want to remove ${stockName} from watchlist")
                            // update button label
                            updateWatchListButtonLabel();
                        });
                }
            });
    }

    /*
        update the button's name
     */
    function updateWatchListButtonLabel() {
        $.post("/watchlist/check",
            {"symbol": "${stockName}"},
            function(result){
                     $("#add-to-watchlist").text(
                        result == "true" ? "In watchlist" : "Add to watchlist")


            });
    }


    /*
       set timestamp
   */
    window.setInterval(function () {
        /*alert("requesting new prices...")*/
        reqPrice();
        reqRealTimePrice();
    }, 60000);


    /*
         keep symbol uppercase status
     */
    $("#search-stock-field").keypress(function(change_to_uppercase) {
        if ($(this).val().length < 20) {
            var keyboard_alpha_code = change_to_uppercase.which;
            if (keyboard_alpha_code >= 97 && keyboard_alpha_code <= 122) {
                change_to_uppercase.preventDefault();
                $(this).val($(this).val() + String.fromCharCode(keyboard_alpha_code - 32));
            }
        }
        else {
            $(this).val($(this).val());
        }
    });


	/*
       search function
   */
    $(document).ready(function search_symbol() {

        $("#search-btn").click(
            function (){
                var stocksearch=$("#search-stock-field").val()
                window.location.href="/quote?stockName="+stocksearch;
            });

        updateWatchListButtonLabel();

        reqPrice();

        reqRealTimePrice();

    });



    /*
         chart part
     */
    function reqRealTimePrice() {
        var req = $.getJSON(
            VARS.API_URL,
            {
                'function': 'TIME_SERIES_DAILY_ADJUSTED',
                symbol: "${stockName}",
                apikey: VARS.API_KEY
            },
            function (resJson, status, jqXHR) {
                if (resJson) {

                    var feedToHighcharts = [];

                    var many = resJson['Time Series (Daily)'];
                    if (many)
                        for (var date in many) {
                            var point = []
                            var one = many[date];

                            point.push(Date.parse(date))
                            point.push(JSON.parse(one['1. open']))


                            feedToHighcharts.push(point)
                        }
                    feedToHighcharts.reverse()

                    Highcharts.stockChart('container', {
                        rangeSelector : {
                            selected : 1,
                            allButtonsEnabled: true
                        },
                        title : {
                            text : "${stockName} Stock Price"
                        },
                        series : [ {
                            name : "${stockName}",
                            data : feedToHighcharts,
                            tooltip : {
                                valueDecimals : 2
                            }
                        } ]
                    });
                }
            })
    }


    /*
      like part
    */

    $(function () {
        $(".like").click(function () {
            $(this).toggleClass('do_like');
        })
    })

</script>
</body>
</html>

