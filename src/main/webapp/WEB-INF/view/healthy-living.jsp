<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Healthy Living</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<div class="container">
    <div id="carouselExampleIndicators" class="carousel slide col-lg-8 border" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img style="width:100%;height:380px"
                     src="https://s.yimg.com/dh/ap/finance/portfolio/healthy-living.jpg"
                     alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Healthy Living</h5><button class="btn" id="watchlist-add">Add to My Watchlist</button>
                    <p>This basket consists of stocks gaining popularity from health and wellness.</p>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top:2rem">
        <em>Background</em>
        <p>While America is notorious for its soaring obesity rates, Gallup found last year that people in the US have
            been exercising more frequently in the past few years. The companies on this list track fitness goals,
            provide workout gear, and supply nutritious food for Americans craving a healthy lifestyle.</p>
        <em>How did we choose these stocks?</em>
        <p>Each of these stocks was chosen by the Yahoo Finance editorial staff.</p>
        <em>Who made these selections?</em>
        <p>Yahoo Finance is the most-read business website in the US, garnering roughly 75 million unique visitors every
            month. The site has extensive coverage of the markets, travel, technology and general business.</p>
        <em>How are these weighted?</em>
        <p>The stocks in this watchlist are weighted equally.</p>
    </div>
    <h3>Symbols</h3>
    <jsp:include page="symbollist.jsp"/>
</div>
<script>
    $('#watchlist-add').click(function () {
        $.post('/watchlist/add-watch',{'name':'healthy-living'});
    })
</script>
</body>

</html>
