<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <script src="/resources/js/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="navbars.jsp"/>
<script>

    $(function () {
        var searchParams = new URLSearchParams(window.location.search);
        if (searchParams.has("symbol")) {
            $('#symbol').val(searchParams.get("symbol"));
        }
        searchList(1);
    });
    var ctx = "${pageContext.request.contextPath}";

    function searchList(pageNo) {
        var company = $("#company").val();
        var symbol = $("#symbol").val();
        $("#loading").show();
        $.ajax({
            type: 'get',
            url: ctx + '/search/show',
            data: {
                symbol: symbol,
                company: company,
                pageNo: pageNo
            },
            cache: false,
            dataType: 'html',
            success: function (result) {
                $("#loading").hide();
                $("#PortfolioList").empty();
                $("#PortfolioList").html(result);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#loading").hide();
            }
        });
    }
</script>

<div class="container">
    <div>
        <h2>Search Result</h2>

        <div class="pt-r pt-ss">
            <div>
                <table>
                    <tr align="center">
                        <td> Company:</td>
                        <td><input id="company" name="company" type="text" class="form-control"
                                   placeholder="please enter company name" onFocus="{this.style.color='#000'}"
                                   onBlur="{this.style.color='#999'}" style="color:#999999"></td>
                        <td>Symbol:</td>
                        <td><input id="symbol" name="symbol" type="text" class="form-control"
                                   placeholder="please enter symbol" onFocus="{this.style.color='#000'}"
                                   onBlur="{this.style.color='#999'}" style="color:#999999"></td>
                        <td><a href="#" class="tooltip-error" style="color:#12AFCB;margin:0 5px;" data-rel="tooltip"
                               onclick="searchList(1);">Search</a></td>
                    </tr>
                    <tr></tr>
                </table>
            </div>
        </div>
    </div>
    <div id="loading" class="loading">Loading...</div>
    <div id="PortfolioList"></div>
</div>
</body>

</html>
