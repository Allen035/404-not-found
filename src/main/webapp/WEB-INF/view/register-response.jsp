<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register Response</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
<div class="section">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">${result}</h1>
            <p class="lead">${message}</p>
        </div>
        <div class="container">
            <p class="lead"><a href="/login">Go to Login</a></p>
        </div>
    </div>
</div>
<script src="/resources/js/jquery-3.3.1.min.js"></script>
<script src="/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>
