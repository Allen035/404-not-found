package com.portfolio.management.interceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginHandlerInterceptor implements HandlerInterceptor{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
//        String reqUrl = request.getRequestURI().replace(request.getContextPath(), "");
//        if(reqUrl.contains("login") || reqUrl.contains("register") || reqUrl.contains("test")){
//            return true;
//        }
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for (Cookie cookie:cookies) {
                String cookieName = cookie.getName();
                if(cookieName.equals("USERNAME"))
                    return true;
            }
        }
        response.sendRedirect("/login");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

    }
}
