package com.portfolio.management.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
    @RequestMapping(value = "/home", method = RequestMethod.GET)    // maps url request
    public String show() {
        return "home";     // tell view resolver which jsp file to render
    }
}
