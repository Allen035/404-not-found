package com.portfolio.management.controller;

import com.alibaba.fastjson.JSON;
import com.portfolio.management.model.Portfolio;
import com.portfolio.management.service.PortfolioService;
import com.portfolio.management.service.UserService;
import com.portfolio.management.util.alphavantage.AlphavantageParserUtil;
import com.portfolio.management.util.alphavantage.TimeSeriesDailyStock;
import com.portfolio.management.util.alphavantage.TimeSeriesIntradayStock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class PortfolioController {

    @Autowired
    private PortfolioService portfolioService;
    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(PortfolioController.class);

    /*private static final int INTERVAL = 1;
    private static final String OUTPUT_SIZE = "compact";
    private static final String DATA_TYPE = "json";
    private static final String API_KEY = "X8RO597ZV071YDUA";*/


    @RequestMapping(value = "/portfolio", method = RequestMethod.GET)
    public ModelAndView portfolio(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("portfolio");
        try {
            String userId = getCurrentUserId(request);
            List<String> symbols = getMyWatchSymbols(request);
            symbols = null == symbols ? new ArrayList<>() : symbols;
            List<List<Map>> lotss = new ArrayList<>();
            symbols.forEach((symbol) ->
                    lotss.add(portfolioService.queryLotsByUserIdAndSymbol(userId, symbol))
            );
            mav.addObject("symbols", symbols);
            mav.addObject("symbolsJson", JSON.toJSONString(symbols));
            mav.addObject("lotss", lotss);
            mav.addObject("lotssJson", JSON.toJSONString(lotss));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return mav;
    }

    @RequestMapping(value="/portfolio/symbol/add", method = RequestMethod.GET)
    public long addSymbol(HttpServletRequest request, String userId, String symbol){
        Portfolio portfolio = new Portfolio();
        portfolio.setUserId(userId);
        portfolio.setSymbol(symbol);
        return portfolioService.insertOne(portfolio);
    }

    @RequestMapping(value = "/portfolio/lots/add")
    @ResponseBody
    public long addALot(Portfolio portfolio, HttpServletRequest request) {
        try {
            if (portfolio != null) portfolio.setUserId(getCurrentUserId(request));
            if (portfolioService.insertOne(portfolio) > 0)
                return portfolio.getId();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return 0;
    }

    @RequestMapping(value = "/portfolio/lots/update")
    @ResponseBody
    public long updateOne(Portfolio portfolio) {
        try {
            return portfolioService.updateOne(portfolio);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return 0;
    }

    @RequestMapping(value = "/portfolio/lots/delete")
    @ResponseBody
    public long deleteOne(Portfolio portfolio) {
        try {
            return portfolioService.deleteOne(portfolio);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return 0;
    }


    private String getCurrentUserId(HttpServletRequest request) {
        return userService.getCurrentUserLogin(request);
    }

    private List<String> getMyWatchSymbols(HttpServletRequest request) {
        try {
            return portfolioService.queryWatchSymbol(getCurrentUserId(request));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return null;
    }
}
