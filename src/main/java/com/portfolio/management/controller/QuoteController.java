package com.portfolio.management.controller;

import com.portfolio.management.dao.StockDao;
import com.portfolio.management.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.portfolio.management.service.UserService;
import com.portfolio.management.service.WatchListService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@Controller
public class QuoteController {
	
    @Autowired
    private WatchListService watchListService;
    @Autowired
    private UserService userService;
    @Autowired
    private StockDao stockDao;
    
    @RequestMapping(value = "/quote",method = RequestMethod.GET)
    public ModelAndView showQuote(HttpServletRequest request){
        // request: "http://localhost:8080/quote?stockName=AAPL"
        String stockNameValue = request.getParameter("stockName");
        
        String userLogin = userService.getCurrentUserLogin(request);
        
        List<String> symbolList =watchListService.queryWatchedStocks(userLogin);
        boolean isInWatchlist = false;
        for(String s: symbolList) {
        	if(stockNameValue != null && stockNameValue.equals(s)) {
        		isInWatchlist = true;
        		break;
        	}
        }

        String stockLongName = stockDao.queryCompanyBySymbol(stockNameValue);

        ModelAndView mv = new ModelAndView("quote");
        mv.addObject("stockName", stockNameValue);
        mv.addObject("isInWatchlist", isInWatchlist);
        mv.addObject("stockLongName", stockLongName);
        return mv;
    }
}
