package com.portfolio.management.controller;

import com.portfolio.management.dao.WatchListDao;
import com.portfolio.management.model.RealTimeStock;
import com.portfolio.management.model.Watchlist;
import com.portfolio.management.service.UserService;
import org.apache.commons.lang.WordUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import com.portfolio.management.service.StockService;
import com.portfolio.management.service.WatchListService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.servlet.ModelAndView;

import com.portfolio.management.util.alphavantage.AlphavantageParserUtil;
import com.portfolio.management.util.alphavantage.StockQuote;

import javax.servlet.http.HttpServletRequest;

@Controller
public class WatchlistController {
    @Autowired
    private StockService stockService;
    @Autowired
    private WatchListService watchListService;
    @Autowired
    private UserService userService;
    @Autowired
    private WatchListDao watchListDao;
    @Autowired
    private AlphavantageParserUtil alphavantageParserUtil;

    private static final Logger logger = LoggerFactory.getLogger(WatchlistController.class);

    @RequestMapping(value = "/watchlist", method = RequestMethod.GET)
    public ModelAndView toShow(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("watchlist");
        List<String> hyphenWatchlists = watchListDao.queryUserWatchList(userService.getCurrentUserLogin(request));
        List<Watchlist> watchlists = new ArrayList<Watchlist>();
        for(String hyphenWatchlist: hyphenWatchlists){
            Watchlist watchlist = new Watchlist();
            watchlist.setHyphenName(hyphenWatchlist);
            watchlist.setName(convertFromHyphen(hyphenWatchlist));
            watchlists.add(watchlist);
        }

        mav.addObject("watchlists",watchlists);
        return mav;
    }

    @RequestMapping(value="/watchlist/detail", method = RequestMethod.GET)
    public ModelAndView showWatchListDetail(String w){
        Integer pageNo = 1;
        Integer pageStart = (pageNo-1)*10;
        Integer pageEnd = pageNo*10;
        List<String> stockList = watchListDao.queryWatchListSymbols(w);

        Integer totalCount = stockList.size();
        List<RealTimeStock> list= alphavantageParserUtil.getRealTimeStock(stockList);
        ModelAndView mv = new ModelAndView(w);
        mv.addObject("list", list);
        mv.addObject("pageNo", pageNo);
        mv.addObject("pageSize", 20);
        mv.addObject("totalCount", totalCount);
        return mv;
    }

    @RequestMapping(value = "/watchlist/add-watch", method = RequestMethod.POST)
    public void addWatchlist(HttpServletRequest request, String name){
        String login = userService.getCurrentUserLogin(request);
        watchListDao.addUserWatchlist(login, name);
    }

    @RequestMapping(value = "/watchlist/add", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void addSymbolByUser(HttpServletRequest request, String symbol){
        String userId = userService.getCurrentUserLogin(request);
        watchListService.addSymbol(userId, symbol);
    }

    @RequestMapping(value = "/watchlist/remove", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeSymbolByUser(HttpServletRequest request, String symbol){
        String userId = userService.getCurrentUserLogin(request);
        watchListService.removeSymbol(userId, symbol);
    }

    @RequestMapping(value = "/watchlist/check", method = RequestMethod.POST)
    @ResponseBody
    public String checkSymbol(HttpServletRequest request, String symbol){
        String userLogin = userService.getCurrentUserLogin(request);
        List<String> watchedSymbols = watchListService.queryWatchedStocks(userLogin);
        for(String s: watchedSymbols) {
            if(symbol != null && symbol.equals(s))
                return "true";
        }
        return "false";
    }

    public String convertFromHyphen(String hyphenName){
        if(hyphenName.contains("-")){
            return WordUtils.capitalize(hyphenName.replace("-"," "));
        }
        return hyphenName;
    }
}
