package com.portfolio.management.controller;

import com.portfolio.management.dao.StockDao;
import com.portfolio.management.dao.WatchListDao;
import com.portfolio.management.model.PerformanceAnalysisStock;
import com.portfolio.management.model.RealTimeStock;
import com.portfolio.management.service.UserService;
import com.portfolio.management.util.alphavantage.AlphavantageParserUtil;
import com.portfolio.management.util.alphavantage.AlphavantageUtil;
import com.portfolio.management.util.alphavantage.TimeSeriesDailyAdjustedStock;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Controller
public class PerformanceAnalyticsController {
    @Autowired
    RecommendationController recommendationController;
    @Autowired
    WatchListDao watchListDao;
    @Autowired
    UserService userService;
    @Autowired
    AlphavantageParserUtil alphavantageParserUtil;
    @Autowired
    StockDao stockDao;
    private final Logger LOGGER = Logger.getLogger(PerformanceAnalyticsController.class);
    private static final String API_KEY = "X8RO597ZV071YDUA";

    private final List<String> MEMO = Arrays.asList(
            "Low returns, high volatility",
            "High prices",
            "High returns, high volatility",
            "Low returns, low volatility",
            "High returns, low volatility",
            "Mild returns, mild volatility");

    @RequestMapping(value = "/performance-analytics")
    public ModelAndView show(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("performance-analytics");
        List<String> symbols = watchListDao.queryWatchSymbol(userService.getCurrentUserLogin(request),
                "", "", 0, 10);
        Integer pageNo = 1;
        Integer pageStart = (pageNo - 1) * 10;
        Integer pageEnd = pageNo * 10;

        Integer totalCount = symbols.size();
        List<PerformanceAnalysisStock> list = getPerformanceAnalysisStock(symbols);

        mv.addObject("list", list);
        mv.addObject("pageNo", pageNo);
        mv.addObject("pageSize", 20);
        mv.addObject("totalCount", totalCount);

        return mv;

    }

    public List<PerformanceAnalysisStock> getPerformanceAnalysisStock(List<String> symbols){
        ExecutorService executorService = Executors.newCachedThreadPool();
        List<PerformanceAnalysisStock> performanceAnalysisStocks = new ArrayList<>();
        for(String symbol: symbols){
            PerformanceAnalysisStock performanceAnalysisStock = new PerformanceAnalysisStock();
            performanceAnalysisStock.setSymbol(symbol);
            performanceAnalysisStocks.add(performanceAnalysisStock);

            // query 3 apis in parallel using multithreading
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    LOGGER.info(Thread.currentThread().getName());
                    List<TimeSeriesDailyAdjustedStock> stocks = AlphavantageParserUtil.parseTimeSeriesDailyAdjusted(symbol,"","",API_KEY);
                    stocks.sort(Comparator.comparing(TimeSeriesDailyAdjustedStock::getTime));
                    Collections.reverse(stocks);
                    TimeSeriesDailyAdjustedStock todayStock = stocks.get(0);
                    Float todayPrice = Float.parseFloat(todayStock.getAdjustedClose());
                    Float yesterdayPrice = Float.parseFloat((stocks.get(1).getAdjustedClose()));
                    String company = stockDao.queryCompanyBySymbol(symbol);

                    performanceAnalysisStock.setSymbol(todayStock.getSymbol());
                    performanceAnalysisStock.setVolume(todayStock.getVolume());
                    performanceAnalysisStock.setPrice(todayPrice);
                    performanceAnalysisStock.setChange(todayPrice - yesterdayPrice);
                    performanceAnalysisStock.setStyledChange(AlphavantageParserUtil.getStyledChange(todayPrice - yesterdayPrice));
                    performanceAnalysisStock.setPercentageChange((todayPrice-yesterdayPrice)/yesterdayPrice*100);
                    performanceAnalysisStock.setStyledPercentageChange(AlphavantageParserUtil.getStyledChange((todayPrice-yesterdayPrice)/yesterdayPrice*100));
                    if(company!=null)
                        performanceAnalysisStock.setCompany(company);
                }
            });
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    LOGGER.info(Thread.currentThread().getName());
                    Integer label = recommendationController.getLabel(symbol);
                    performanceAnalysisStock.setLabel(label);
                    performanceAnalysisStock.setShortTerm(MEMO.get(label));
                }
            });
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    LOGGER.info(Thread.currentThread().getName());
                    performanceAnalysisStock.setRsi(AlphavantageParserUtil.parseRSI(symbol));
                }
            });

        }
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(10, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            return performanceAnalysisStocks;
        }
        return performanceAnalysisStocks;
    }


}
