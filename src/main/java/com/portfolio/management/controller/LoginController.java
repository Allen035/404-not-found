package com.portfolio.management.controller;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.portfolio.management.model.Login;
import com.portfolio.management.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.portfolio.management.service.UserService;

import java.io.IOException;

@Controller
public class LoginController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("login", new Login());
        return mav;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView loginProcess(HttpServletResponse response, @ModelAttribute("login") Login login) throws IOException {

        ModelAndView mav = null;
        User user = userService.validateUser(login);
        if (user != null) {
            mav = new ModelAndView("redirect:/home");
            Cookie cookie = new Cookie("USERNAME", login.getLogin()); //bake cookie
            cookie.setMaxAge(24 * 60 * 60); //set expire time to 1 day
            response.addCookie(cookie); //put cookie in response
        } else {
            mav = new ModelAndView("login");
            mav.addObject("message", "Username or Password is wrong!!");
        }
        return mav;
    }
}
