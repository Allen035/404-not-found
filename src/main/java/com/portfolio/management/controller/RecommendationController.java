package com.portfolio.management.controller;

import com.portfolio.management.dao.StockDao;
import com.portfolio.management.dao.WatchListDao;
import com.portfolio.management.model.SymbolMetrics;
import com.portfolio.management.service.UserService;
import com.portfolio.management.util.alphavantage.AlphavantageParserUtil;
import com.portfolio.management.util.alphavantage.TimeSeriesWeeklyAdjustedStock;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.util.*;
import com.portfolio.management.util.StatisticsUtil;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RecommendationController {
    @Autowired
    private WatchListDao watchListDao;
    @Autowired
    private UserService userService;
    @Autowired
    private StockDao stockDao;
    private static final String API_KEY = "V8XAHB694RQWKB4F";
    private static final String DATA_DIR = "training_data.csv";
    private static final String CACHE_DIR = "cache_data.csv";
    private static final Logger LOGGER = Logger.getLogger(RecommendationController.class);

    @RequestMapping(value = "/recommendation", method = RequestMethod.GET)
    public ModelAndView show(HttpServletRequest request){
        ModelAndView mav = new ModelAndView("recommendation");
        List<String> symbols = watchListDao.queryWatchSymbol(userService.getCurrentUserLogin(request),
                "","",0,10);
        List<SymbolMetrics> recommendations = getRecommendations(symbols);
        List<String> companies = new ArrayList<>();
        for(SymbolMetrics rec: recommendations){
            companies.add(stockDao.queryCompanyBySymbol(rec.getSymbol()));
        }
        mav.addObject("recommendations",recommendations);
        mav.addObject("companies",companies);
        return mav;
    }

    public Integer getLabel(String symbol){
        List<SymbolMetrics> sampleMetrics = StatisticsUtil.readSymbolMetricsCSV(DATA_DIR);
        Integer label = getDirectLabel(symbol, sampleMetrics);
        if(label != null)
            return label;
        SymbolMetrics symbolMetric = getMetrics(symbol);
        Double min = Double.POSITIVE_INFINITY;
        SymbolMetrics minSymbolMetric = new SymbolMetrics();
        for(SymbolMetrics sampleMetric: sampleMetrics){
            Double dist = calcSymbolMetricDistance(sampleMetric, symbolMetric);
            if(dist < min){
                min = dist;
                minSymbolMetric = sampleMetric;
            }
        }
        return minSymbolMetric.getLabel();


    }

    public List<SymbolMetrics> getRecommendations(List<String> symbols){

        // store all symbolMetrics from symbols from param
        List<SymbolMetrics> symbolMetrics = new ArrayList<>();
        if(symbols.size() == 0)
            return symbolMetrics;
        // store uncached symbolMetrics
        List<SymbolMetrics> uncachedSymbolMetrics = new ArrayList<>();

        // implement KNN(neighbour=1) for each symbol to get their labels
        List<Integer> labels = new ArrayList<>();

        List<SymbolMetrics> sampleMetrics = StatisticsUtil.readSymbolMetricsCSV(DATA_DIR);


        // add label to labels immediately if it is in training data or cache data
        // or keep calculating metrics
        for(String symbol: symbols){
            SymbolMetrics sampleMetric = getDirectMetric(symbol, sampleMetrics);
            if(sampleMetric!=null){
                labels.add(sampleMetric.getLabel());
                symbolMetrics.add(sampleMetric);
                continue;
            }
            SymbolMetrics metric = getMetrics(symbol);
            uncachedSymbolMetrics.add(metric);
            symbolMetrics.add(metric);
        }

        // calculate minimum hamming distance with training data symbols
        // pick the label with minimum distance
        for(SymbolMetrics symbolMetric: uncachedSymbolMetrics){
            Double min = Double.POSITIVE_INFINITY;
            SymbolMetrics minSymbolMetric = new SymbolMetrics();
            for(SymbolMetrics sampleMetric: sampleMetrics){
                Double dist = calcSymbolMetricDistance(sampleMetric, symbolMetric);
                if(dist < min){
                    min = dist;
                    minSymbolMetric = sampleMetric;
                }
            }
            labels.add(minSymbolMetric.getLabel());
        }

        // get mode number(most frequent) of labels
        Integer modeLabel = StatisticsUtil.getMode(labels);
        HashMap<Integer,ArrayList<SymbolMetrics>> hashMap = getSymbolMetricMap(sampleMetrics);
        ArrayList<SymbolMetrics> labelArray = hashMap.get(modeLabel);
        List<SymbolMetrics> recommendedSymbolMetrics = new ArrayList<>();
        List<String> recommendations = new ArrayList<String>();

        // randomly recommend 5 symbols of that most frequent label
        new Random().ints(0, labelArray.size()).limit(5).forEach(
                i -> recommendedSymbolMetrics.add(labelArray.get(i)));
        for(SymbolMetrics rec: recommendedSymbolMetrics){
            rec.setSimilarity(calcSymbolMetricSimilarity(rec, symbolMetrics));
        }
        return recommendedSymbolMetrics;
    }

    public Integer getDirectLabel(String symbol, List<SymbolMetrics> metrics){
        for(SymbolMetrics metric: metrics){
            if(metric.getSymbol().equals(symbol)){
                return metric.getLabel();
            }
        }
        return null;
    }

    public SymbolMetrics getDirectMetric(String symbol, List<SymbolMetrics> metrics){
        for(SymbolMetrics metric: metrics){
            if(metric.getSymbol().equals(symbol)){
                return metric;
            }
        }
        return null;
    }

    public HashMap<Integer,ArrayList<SymbolMetrics>> getSymbolMetricMap(List<SymbolMetrics> sampleMetrics){
        HashMap<Integer,ArrayList<SymbolMetrics>> hashMap = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            hashMap.put(i,new ArrayList<SymbolMetrics>());
        }
        for(SymbolMetrics sampleMetric: sampleMetrics){
            ArrayList<SymbolMetrics> labelArray = hashMap.get(sampleMetric.getLabel());
            labelArray.add(sampleMetric);
        }
        return hashMap;
    }

    public Double calcSymbolMetricDistance(SymbolMetrics sm1, SymbolMetrics sm2){
        return Math.abs(sm1.getReturns() - sm2.getReturns()) +
                Math.abs(sm1.getVolatility() - sm2.getVolatility()) +
                Math.abs(sm1.getAverage() - sm2.getAverage());
    }

    private Double calcSymbolMetricSimilarity(SymbolMetrics sm, List<SymbolMetrics> symbolMetrics){
        Double dist = 10.0;
        for(SymbolMetrics symbolMetric: symbolMetrics){
            dist += (sm.getReturns() - symbolMetric.getReturns()) +
                    (sm.getVolatility() - symbolMetric.getVolatility()) +
                    (sm.getAverage() - symbolMetric.getAverage())/10.0;
        }
        Double candidateSim = Math.round(Math.abs(3000.0/dist)*100.0)/100.0;
        if(candidateSim > 95.0)
            candidateSim = 95.0;
        return candidateSim;
    }

    public SymbolMetrics getMetrics(String symbol){
        List<TimeSeriesWeeklyAdjustedStock> weeklyStocks = AlphavantageParserUtil.parseTimeSeriesWeeklyAdjusted(
                symbol, "", API_KEY);
        weeklyStocks.sort(Comparator.comparing(TimeSeriesWeeklyAdjustedStock::getTime));
        Collections.reverse(weeklyStocks);
        ArrayList<Double> percentageChanges = new ArrayList<>();
        Double average = 0.0;

        // pick latest 20 weeks
        int size = weeklyStocks.size()>20?20:weeklyStocks.size();
        for (int i = 0; i < size; i++) {
            TimeSeriesWeeklyAdjustedStock stock = weeklyStocks.get(i);
            Double price = Double.parseDouble(stock.getAdjustedClose());
            average += price;
            if(i +1 != size) {
                TimeSeriesWeeklyAdjustedStock stockBefore = weeklyStocks.get(i + 1);
                Double priceBefore = Double.parseDouble(stockBefore.getAdjustedClose());
                Double percentageChange = (price - priceBefore) / priceBefore;
                percentageChanges.add(percentageChange);
            }
        }

        // calculate returns, volatility, average
        Double returns = percentageChanges.stream().mapToDouble(a->a).average().orElse(0.0) * size;
        Double volatilities = StatisticsUtil.getStandardDeviation(percentageChanges) * Math.sqrt((double)size);
        average = average / (double)size;
        SymbolMetrics symbolMetric = new SymbolMetrics();
        symbolMetric.setSymbol(symbol);
        symbolMetric.setAverage(average);
        symbolMetric.setReturns(returns);
        symbolMetric.setVolatility(volatilities);
        return symbolMetric;
    }
}

