package com.portfolio.management.controller;

import com.portfolio.management.model.RealTimeStock;
import com.portfolio.management.service.StockService;
import com.portfolio.management.service.WatchListService;
import com.portfolio.management.util.alphavantage.AlphavantageParserUtil;
import com.portfolio.management.util.alphavantage.StockQuote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class SearchController {
    @Autowired
    private StockService stockService;
    @Autowired
    private WatchListService watchListService;
    @Autowired
    private AlphavantageParserUtil alphavantageParserUtil;

    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public ModelAndView toShow(){
        ModelAndView mav = new ModelAndView("search");
        return mav;
    }

    @RequestMapping(value = "/search/show",method = RequestMethod.GET)
    public ModelAndView show(String company,String symbol,Integer pageNo){
        Integer pageStart = (pageNo-1)*10;
        Integer pageEnd = pageNo*10;
        List<String> stockList = stockService.querySymbol(company, symbol, pageStart, pageEnd);


        Integer totalCount = stockService.querySymbolCount(company,symbol);
        List<RealTimeStock> list= alphavantageParserUtil.getRealTimeStock(stockList);
        ModelAndView mv = new ModelAndView("symbollist");
        mv.addObject("list", list);
        mv.addObject("pageNo", pageNo);
        mv.addObject("pageSize", 10);
        mv.addObject("totalCount", totalCount);

        return mv;
    }
}
