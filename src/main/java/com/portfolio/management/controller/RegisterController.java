package com.portfolio.management.controller;
import org.springframework.beans.factory.annotation.Autowired;
import com.portfolio.management.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
// change portlet.ModelAndView to servlet.ModelAndView
import com.portfolio.management.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class RegisterController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("register");
        mav.addObject("user", new User());
        return mav;
    }

    @RequestMapping(value = "/register/response", method = RequestMethod.POST)
    public ModelAndView addUser(@ModelAttribute("user") User user) {
        boolean registered = userService.register(user);
        String result;
        String message;
        if(registered) {
            result = "Welcome";
            message = user.getName();
        }
        else {
            result = "Sorry";
            message = "You have registered before.";
        }
        ModelAndView mav = new ModelAndView("register-response");
        mav.addObject("result", result);
        mav.addObject("message", message);
        return mav;
    }
}
