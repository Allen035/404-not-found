package com.portfolio.management.model;

import java.util.Objects;


public class PortfolioSymbol {
    private String symbol;
    private String realTimeClose;
    private String dayOpen;

    public PortfolioSymbol() {
    }

    public PortfolioSymbol(String symbol) {
        this.symbol = symbol;
    }

    public PortfolioSymbol(String symbol, String realTimeClose, String dayOpen) {
        this.symbol = symbol;
        this.realTimeClose = realTimeClose;
        this.dayOpen = dayOpen;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRealTimeClose() {
        return realTimeClose;
    }

    public void setRealTimeClose(String realTimeClose) {
        this.realTimeClose = realTimeClose;
    }

    public String getDayOpen() {
        return dayOpen;
    }

    public void setDayOpen(String dayOpen) {
        this.dayOpen = dayOpen;
    }

    @Override
    public String toString() {
        return "PortfolioSymbol{" +
                "symbol='" + symbol + '\'' +
                ", realTimeClose='" + realTimeClose + '\'' +
                ", dayOpen='" + dayOpen + '\'' +
                '}';
    }
}
