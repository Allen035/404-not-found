package com.portfolio.management.model;

/**
 * Auto Generated
 */
public class Portfolio {

    private String notes;
    private double cost_Basis;
    private double shares;
    private double no_Of_Lot;
    private String symbol;
    private String userId;
    private long id;


    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public double getCost_Basis() {
        return cost_Basis;
    }

    public double getCostBasis() {
        return cost_Basis;
    }

    public void setCost_Basis(double cost_Basis) {
        this.cost_Basis = cost_Basis;
    }

    public void setCostBasis(double cost_Basis) {
        this.cost_Basis = cost_Basis;
    }


    public double getShares() {
        return shares;
    }

    public void setShares(double shares) {
        this.shares = shares;
    }


    public double getNo_Of_Lot() {
        return no_Of_Lot;
    }

    public double getNoOfLot() {
        return no_Of_Lot;
    }

    public void setNo_Of_Lot(double no_Of_Lot) {
        this.no_Of_Lot = no_Of_Lot;
    }

    public void setNoOfLot(double no_Of_Lot) {
        this.no_Of_Lot = no_Of_Lot;
    }


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
