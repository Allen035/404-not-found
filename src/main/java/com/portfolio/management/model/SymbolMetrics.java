package com.portfolio.management.model;

public class SymbolMetrics {
    private String symbol;
    private Double returns;
    private Double volatility;
    private Double average;
    private Integer label;
    private Double similarity;

    public Integer getLabel() {
        return label;
    }

    public void setLabel(Integer label) {
        this.label = label;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getReturns() {
        return returns;
    }

    public void setReturns(Double returns) {
        this.returns = returns;
    }

    public Double getVolatility() {
        return volatility;
    }

    public void setVolatility(Double volatility) {
        this.volatility = volatility;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Double similarity) {
        this.similarity = similarity;
    }

    @Override
    public String toString() {
        return "SymbolMetrics{" +
                "symbol='" + symbol + '\'' +
                ", returns=" + returns +
                ", volatility=" + volatility +
                ", average=" + average +
                ", label=" + label +
                ", similarity=" + similarity +
                '}';
    }



}
