package com.portfolio.management.model;

public class Watchlist {
    private String hyphenName;
    private String name;

    public String getHyphenName() {
        return hyphenName;
    }

    public void setHyphenName(String hyphenName) {
        this.hyphenName = hyphenName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
