package com.portfolio.management.model;

public class PerformanceAnalysisStock extends RealTimeStock {
    private String rsi;
    private Integer label;
    private String shortTerm;

    @Override
    public String toString() {
        return "PerformanceAnalysisStock{" +
                "rsi='" + rsi + '\'' +
                ", label=" + label +
                ", shortTerm='" + shortTerm + '\'' +
                '}';
    }

    public String getShortTerm() {
        return shortTerm;
    }

    public void setShortTerm(String shortTerm) {
        this.shortTerm = shortTerm;
    }

    public Integer getLabel() {
        return label;
    }

    public void setLabel(Integer label) {
        this.label = label;
    }

    public String getRsi() {
        return rsi;
    }

    public void setRsi(String rsi) {
        this.rsi = rsi;
    }

}
