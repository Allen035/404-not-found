package com.portfolio.management.model;

public class RealTimeStock {
    private String symbol;
    private String company;
    private Float price;
    private Float change;
    private String styledChange;
    private Float percentageChange;
    private String styledPercentageChange;
    private String volume;

    public String getStyledChange() {
        return styledChange;
    }

    public void setStyledChange(String styledChange) {
        this.styledChange = styledChange;
    }

    public String getStyledPercentageChange() {
        return styledPercentageChange;
    }

    public void setStyledPercentageChange(String styledPercentageChange) {
        this.styledPercentageChange = styledPercentageChange;
    }



    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getChange() {
        return change;
    }

    public void setChange(Float change) {
        this.change = change;
    }

    public Float getPercentageChange() {
        return percentageChange;
    }

    public void setPercentageChange(Float percentageChange) {
        this.percentageChange = percentageChange;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "RealTimeStock{" +
                "symbol='" + symbol + '\'' +
                ", company='" + company + '\'' +
                ", price=" + price +
                ", change=" + change +
                ", percentageChange=" + percentageChange +
                ", volume='" + volume + '\'' +
                '}';
    }
}
