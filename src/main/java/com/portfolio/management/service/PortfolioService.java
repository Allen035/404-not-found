package com.portfolio.management.service;

import com.portfolio.management.dao.PortfolioDao;
import com.portfolio.management.model.Portfolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PortfolioService {
    @Autowired
    private PortfolioDao portfolioDao;

    public List<String> queryWatchSymbol(String userId) {
        return portfolioDao.queryWatchSymbol(userId);
    }

    public List<Map> queryLotsByUserIdAndSymbol(String userId, String symbol) {
        return portfolioDao.queryLotsByUserIdAndSymbol(userId, symbol);
    }

    public long insertOne(Portfolio portfolio) {
        return portfolioDao.insertOne(portfolio);
    }

    public int deleteOne(Portfolio portfolio) {
        return portfolioDao.deleteOne(portfolio);
    }

    public int updateOne(Portfolio portfolio) {
        return portfolioDao.updateOne(portfolio);
    }

}
