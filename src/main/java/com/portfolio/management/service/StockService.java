package com.portfolio.management.service;

import com.portfolio.management.dao.StockDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StockService {
    @Autowired
    private StockDao stockDao;

    /**
     * 根据参数分页查询Symbol
     * @param company
     * @param symbol
     * @param pageStart
     * @param pageEnd
     * @return
     */
    public List<String> querySymbol (String company,String symbol,Integer pageStart,Integer pageEnd ){
        return stockDao.querySymbol( company, symbol, pageStart, pageEnd );
    }

    /**
     * 根据条件查总数
     * @param company
     * @param symbol
     * @return
     */
    public Integer querySymbolCount (String company,String symbol){
        return stockDao.querySymbolCount( company, symbol);
    }
}
