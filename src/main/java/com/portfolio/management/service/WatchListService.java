package com.portfolio.management.service;

import com.portfolio.management.dao.WatchListDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class WatchListService {
    @Autowired
    private WatchListDao watchListDao;

    /**
     * 根据参数分页查询Symbol
     * @param company
     * @param symbol
     * @param pageStart
     * @param pageEnd
     * @return
     */
    public List<String> queryWatchSymbol (String userId,String company,String symbol,Integer pageStart,Integer pageEnd ){
        return watchListDao.queryWatchSymbol( userId,company, symbol, pageStart, pageEnd );
    }

    /**
     * 根据条件查总数
     * @param company
     * @param symbol
     * @return
     */
    public Integer queryWatchSymbolCount (String userId,String company,String symbol){
        return watchListDao.queryWatchSymbolCount( userId,company, symbol);
    }

    public List<String> queryWatchedStocks(String userLogin){
        return watchListDao.queryWatchedStocks(userLogin);
    }

    public void addSymbol(String userId, String symbol){
        watchListDao.addSymbol(userId, symbol);
    }

    public void removeSymbol(String userId, String symbol){
        watchListDao.removeSymbol(userId, symbol);
    }
}
