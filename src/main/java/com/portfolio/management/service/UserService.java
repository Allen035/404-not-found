package com.portfolio.management.service;

import com.portfolio.management.dao.impl.UserImpl;
import com.portfolio.management.model.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.portfolio.management.dao.impl.mapper.UserMapper;
import com.portfolio.management.model.User;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class UserService {
    private JdbcTemplate jdbcTemplateObject;


    @Autowired
    public void setJdbcTemplateObject(JdbcTemplate jdbcTemplateObject) {
        this.jdbcTemplateObject = jdbcTemplateObject;
    }

    public boolean register(User user) {
        boolean userExists = this.checkAvailability(user.getLogin());
        if (userExists) {
            return false;
        }
        String sql = "INSERT INTO Users(login,`name`,pswd) VALUES(?,?,?)";
        jdbcTemplateObject.update(sql, user.getLogin(), user.getName(), user.getPswd());
        return true;
    }


    public User validateUser(Login login) {
        String sql = "select * from Users where login=? and pswd=?; ";
        List<User> users = jdbcTemplateObject.query(sql,
                new Object[]{login.getLogin(), login.getPassword()}, new UserMapper());
        return users.size() > 0 ? users.get(0) : null;
    }

    public User validateUser(String login, String password) {
        String sql = "select * from Users where login=? and pswd=?; ";
        List<User> users = jdbcTemplateObject.query(sql,
                new Object[]{login, password}, new UserMapper());
        return users.size() > 0 ? users.get(0) : null;
    }

    public boolean checkAvailability(String login){
        String sql = "select * from Users where login=?; ";
        List<User> users = jdbcTemplateObject.query(sql,
                new Object[]{login}, new UserMapper());
        return users.size() > 0;
    }

    public String getCurrentUserLogin(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for (Cookie cookie:cookies) {
                String cookieName = cookie.getName();
                if(cookieName.equals("USERNAME")){
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public Integer getCurrentUserId(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for (Cookie cookie:cookies) {
                String cookieName = cookie.getName();
                if(cookieName.equals("USERNAME")){
                    String username = cookie.getValue();
                    String sql = "select ID from Users where login=?;";
                    Integer id = jdbcTemplateObject.queryForObject(sql,new Object[]{username},Integer.class);
                    return id;
                }
            }
        }
        return null;
    }
}

