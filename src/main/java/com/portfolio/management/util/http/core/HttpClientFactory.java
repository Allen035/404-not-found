package com.portfolio.management.util.http.core;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;


public class HttpClientFactory {

    private static final int connectTimeout           = 90000;
    private static final int socketTimeout            = 90000;
    private static final int connectionRequestTimeout = 90000;

    private static final int maxTotalConns            = 2000;
    private static final int defaultMaxConnsPerRoute  = 100;


	public static CloseableHttpClient getCloseableHttpClient() {

        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connectTimeout)
                .setSocketTimeout(socketTimeout).setConnectionRequestTimeout(connectionRequestTimeout)
                .setStaleConnectionCheckEnabled(true).build();

        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(maxTotalConns);
        cm.setDefaultMaxPerRoute(defaultMaxConnsPerRoute);


        return HttpClients.custom().setDefaultRequestConfig(requestConfig).setConnectionManager(cm).build();

    }

}
