package com.portfolio.management.util.http.core;

import java.io.IOException;

import com.portfolio.management.util.result.CommonResultCode;
import com.portfolio.management.util.result.PlainResult;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class HttpResponseHandler implements ResponseHandler<PlainResult<String>> {
    private static final Logger  logger   = LoggerFactory.getLogger(HttpResponseHandler.class);

    @Override
    public PlainResult<String> handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
        PlainResult<String> result = new PlainResult<String>();

        final StatusLine statusLine = response.getStatusLine();
        logger.info("http response code：{}",statusLine.getStatusCode());
        
        final HttpEntity entity = response.getEntity();
       
        if (statusLine.getStatusCode() < 200 || statusLine.getStatusCode() >= 300) {
            EntityUtils.consume(entity);
            return new PlainResult<String>().setErrorMessage(CommonResultCode.FAIL_HTTP_CALL,
                    statusLine.getReasonPhrase(), statusLine.getStatusCode());
        }
        
        String content=null;
        if(entity!=null){

        	content= EntityUtils.toString(entity,"utf-8");
        }
        result.setData(content);    
        logger.info("http响应的返回值：{}",content);
        return result;
    }

};
