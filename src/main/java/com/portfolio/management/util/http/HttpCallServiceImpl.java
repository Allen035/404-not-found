package com.portfolio.management.util.http;

import org.apache.http.client.ResponseHandler;
import java.util.Map;

import com.portfolio.management.util.http.core.HttpGetUtil;
import com.portfolio.management.util.http.core.HttpPostUtil;
import com.portfolio.management.util.http.core.HttpResponseHandler;
import com.portfolio.management.util.result.PlainResult;





public class HttpCallServiceImpl implements HttpCallService {

    private static final ResponseHandler<PlainResult<String>> defaultResponseHandler = new HttpResponseHandler();

    @Override
    public PlainResult<String> httpGet(String requestUri) {
        return HttpGetUtil.execute(requestUri, defaultResponseHandler);
    }

    @Override
    public PlainResult<String> httpGet(String requestUri, Map<String, String> params) {
        return HttpGetUtil.execute(requestUri, params, defaultResponseHandler);
    }

    @Override
    public PlainResult<String> httpPost(String requestUri) {
        return HttpPostUtil.execute(requestUri, null, defaultResponseHandler);
    }

    @Override
    public PlainResult<String> httpPost(String requestUri, Map<String, String> params) {
        return HttpPostUtil.execute(requestUri, params, defaultResponseHandler);
    }
}
