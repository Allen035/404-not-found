package com.portfolio.management.util.http;

import java.util.Map;

import com.portfolio.management.util.result.PlainResult;



public interface HttpCallService {

    PlainResult<String> httpGet(String requestUri);

    PlainResult<String> httpGet(String requestUri, Map<String, String> params);

    PlainResult<String> httpPost(String requestUri);

    PlainResult<String> httpPost(String requestUri, Map<String, String> params);
}
