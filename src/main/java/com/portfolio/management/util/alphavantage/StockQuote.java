package com.portfolio.management.util.alphavantage;

import java.time.LocalDateTime;


public class StockQuote {
    private String symbol;
    private String price;
    private String volume;
    private LocalDateTime timestamp;
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "StockQuote{" +
                "symbol='" + symbol + '\'' +
                ", price='" + price + '\'' +
                ", volume='" + volume + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
