package com.portfolio.management.util.alphavantage;

import java.time.LocalDateTime;

public class TimeSeriesIntradayStock extends Stock {
    private String symbol;
    private String interval;
    private LocalDateTime time;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "TimeSeriesIntradayStock{" +
                "symbol='" + symbol + '\'' +
                ", interval='" + interval + '\'' +
                ", time=" + time +
                '}';
    }
}
