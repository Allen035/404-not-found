package com.portfolio.management.util.alphavantage;

import java.time.LocalDate;


public class TimeSeriesWeeklyAdjustedStock extends Stock {
    private String symbol;
    private LocalDate time;
    private String adjustedClose;
    private String dividendAmount;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public LocalDate getTime() {
        return time;
    }

    public void setTime(LocalDate time) {
        this.time = time;
    }

    public String getAdjustedClose() {
        return adjustedClose;
    }

    public void setAdjustedClose(String adjustedClose) {
        this.adjustedClose = adjustedClose;
    }

    public String getDividendAmount() {
        return dividendAmount;
    }

    public void setDividendAmount(String dividendAmount) {
        this.dividendAmount = dividendAmount;
    }



    @Override
    public String toString() {
        return "TimeSeriesDailyAdjustedStock{" +
                "symbol='" + symbol + '\'' +
                ", time=" + time +
                ", adjustedClose='" + adjustedClose + '\'' +
                ", dividendAmount='" + dividendAmount + '\'' +
                '}';
    }
}
