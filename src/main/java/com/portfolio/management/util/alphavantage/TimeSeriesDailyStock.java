package com.portfolio.management.util.alphavantage;

import java.time.LocalDate;


public class TimeSeriesDailyStock extends Stock {
    private String symbol;
    private LocalDate time;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public LocalDate getTime() {
        return time;
    }

    public void setTime(LocalDate time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "TimeSeriesDailyStock{" +
                "symbol='" + symbol + '\'' +
                ", time=" + time +
                '}';
    }
}
