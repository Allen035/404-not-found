package com.portfolio.management.util.alphavantage;

import com.portfolio.management.util.HttpUtil;
import com.portfolio.management.util.http.HttpCallService;
import com.portfolio.management.util.http.HttpCallServiceImpl;
import com.portfolio.management.util.result.PlainResult;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class AlphavantageUtil {
    private static final Logger LOGGER = Logger.getLogger(AlphavantageUtil.class);
    private static final String BASE_URL = "https://www.alphavantage.co/query";
    private static final String API_KEY = "V8XAHB694RQWKB4F";

    public static String getAll(String symbols){
        Map<String, Object> parameter = new HashMap<String, Object>();
        parameter.put("function","BATCH_STOCK_QUOTES");
        parameter.put("symbols",symbols);
        parameter.put("apikey","V8XAHB694RQWKB4F");
        String url = buildApi(parameter);
        HttpCallService httpCallService = new HttpCallServiceImpl();
        LOGGER.error("the interface req url:"+url);
        PlainResult<String> result =  httpCallService.httpGet(url);
        if(result.isSuccess()){
            return result.getData();
        }else{
            LOGGER.error("ERROR MSG:"+result.getMessage());
            return null;
        }
    }

    /**
     * AlphavantageTimeSeries-Intraday
     *
     * @param symbol
     * @param interval
     * @param outputsize
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getTimeSeriesIntraday(String symbol,String interval,String outputsize,String datatype,String apikey){
        if (StringUtils.isEmpty(symbol) || StringUtils.isEmpty(interval) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbol/interval/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","TIME_SERIES_INTRADAY");
            parameter.put("symbol",symbol);
            parameter.put("interval",interval);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(outputsize)){
                parameter.put("outputsize",outputsize);
            }
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }

    /**
     * AlphavantageTimeSeries-Daily
     *
     * @param symbol
     * @param outputsize
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getTimeSeriesDaily(String symbol,String outputsize,String datatype,String apikey){
        if (StringUtils.isEmpty(symbol) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbol/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","TIME_SERIES_DAILY");
            parameter.put("symbol",symbol);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(outputsize)){
                parameter.put("outputsize",outputsize);
            }
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }

    /**
     * AlphavantageTimeSeries-DailyAdjusted
     *
     * @param symbol
     * @param outputsize
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getTimeSeriesDailyAdjusted(String symbol,String outputsize,String datatype,String apikey){
        if (StringUtils.isEmpty(symbol) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbol/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","TIME_SERIES_DAILY_ADJUSTED");
            parameter.put("symbol",symbol);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(outputsize)){
                parameter.put("outputsize",outputsize);
            }
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }

    /**
     * AlphavantageTimeSeries-Weekly
     *
     * @param symbol
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getTimeSeriesWeekly(String symbol,String datatype,String apikey){
        if (StringUtils.isEmpty(symbol) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbol/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","TIME_SERIES_WEEKLY");
            parameter.put("symbol",symbol);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }
    /**
     * AlphavantageTimeSeries-WeeklyAdjusted
     *
     * @param symbol
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getTimeSeriesWeeklyAdjusted(String symbol,String datatype,String apikey){
        if (StringUtils.isEmpty(symbol) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbol/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","TIME_SERIES_WEEKLY_ADJUSTED");
            parameter.put("symbol",symbol);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }
    /**
     * AlphavantageTimeSeries-Monthly
     *
     * @param symbol
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getTimeSeriesMonthly(String symbol,String datatype,String apikey){
        if (StringUtils.isEmpty(symbol) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbol/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","TIME_SERIES_MONTHLY");
            parameter.put("symbol",symbol);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }
    /**
     * AlphavantageTimeSeries-MonthlyAdjusted
     *
     * @param symbol
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getTimeSeriesMonthlyAdjusted(String symbol,String datatype,String apikey){
        if (StringUtils.isEmpty(symbol) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbol/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","TIME_SERIES_MONTHLY_ADJUSTED");
            parameter.put("symbol",symbol);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }
    /**
     * AlphavantageTimeSeries-BatchStockQuotes
     *
     * @param symbols
     * @param datatype
     * @param apikey
     * @return
     */
    public static String getBatchStockQuotes(String symbols,String datatype,String apikey){
        if (StringUtils.isEmpty(symbols) || StringUtils.isEmpty(apikey)){
            LOGGER.error("The symbols/apikey string is empty!");
            return null;
        }else {
            Map<String, Object> parameter = new HashMap<String, Object>();
            parameter.put("function","BATCH_STOCK_QUOTES");
            parameter.put("symbols",symbols);
            parameter.put("apikey",apikey);
            if (!StringUtils.isEmpty(datatype)){
                parameter.put("datatype",datatype);
            }

            String url = buildApi(parameter);

            return HttpUtil.get(url);
        }

    }
    

    public static String getRSI(String symbol){
        Map<String, Object> parameter = new HashMap<String, Object>();
        parameter.put("function","RSI");
        parameter.put("symbol",symbol);
        parameter.put("interval","daily");
        parameter.put("time_period","10");
        parameter.put("series_type","close");
        parameter.put("apikey",API_KEY);

        String url = buildApi(parameter);
        return HttpUtil.get(url);
    }


    private static String buildApi(Map<String,Object> parameter){
        StringBuilder api = new StringBuilder(BASE_URL)
                .append("?");
        for (Map.Entry<String,Object> entry:parameter.entrySet()){
            api.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        return api.toString();
    }
}
