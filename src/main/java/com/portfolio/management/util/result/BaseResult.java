package com.portfolio.management.util.result;

import java.io.Serializable;


@SuppressWarnings("unchecked")
public class BaseResult implements Serializable {

    private static final long serialVersionUID = 7801949203978416054L;

    public static final BaseResult SUCCESS = new BaseResult();


    private boolean           success;


    private int               code;


    private String            message;

    public BaseResult() {
        this.code = CommonResultCode.SUCCESS.code;
        this.success = true;
        this.message = CommonResultCode.SUCCESS.message;
    }


    public BaseResult(CommonResultCode rc, Object... args) {
        setError(rc, args);
    }


    public <R extends BaseResult> R setErrorMessage(int code, String message) {
        this.code = code;
        this.success = false;
        this.message = message;
        return (R) this;
    }
    
    

    public <R extends BaseResult> R setErrorMessage(String message) {
        this.success = false;
        this.message = message;
        return (R) this;
    }

    public <R extends BaseResult> R setErrorMessage(IErrorCode code, Object... args) {
        this.code = code.getCode();
        this.success = false;
        this.message = String.format(code.getMessage(), args);
        return (R) this;
    }


    public <R extends BaseResult> R setError(IErrorCode errCode, Object... args) {
        this.code = errCode.getCode();
        this.success = false;
        if (args == null || args.length == 0) {
            this.message = errCode.getMessage();
        } else {
            this.message = String.format(errCode.getMessage(), args);
        }
        return (R) this;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
