package com.portfolio.management.util.result;


public interface IErrorCode {

    public int getCode();


    public String getMessage();
}
