package com.portfolio.management.util.result;

import java.util.ArrayList;
import java.util.List;


public class ListResult<T> extends BaseResult {
    private static final long serialVersionUID = 5741020370203813418L;

    private List<T>           data             = new ArrayList<T>();


    private int               count;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
        if (data != null) {
            this.count = data.size();
        }
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
