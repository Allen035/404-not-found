package com.portfolio.management.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;




import java.io.IOException;

public class HttpUtil {
    private static final Logger LOGGER = Logger.getLogger(HttpUtil.class);

    /**
     * get request
     * @param url
     * @return
     */
    public static String get(String url){
        LOGGER.info(url);

        CloseableHttpClient httpCilent = HttpClients.createDefault();

        HttpGet httpGet = new HttpGet(url);

        String strResult = null;
        try {
            HttpResponse httpResponse = httpCilent.execute(httpGet);
            if(httpResponse.getStatusLine().getStatusCode() == 200) {
                strResult = EntityUtils.toString(httpResponse.getEntity());
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }finally {
            try {
                httpCilent.close();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
        return strResult;
    }
}
