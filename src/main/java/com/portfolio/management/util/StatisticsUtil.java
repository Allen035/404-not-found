package com.portfolio.management.util;

import java.io.*;
import java.util.*;

import com.portfolio.management.model.SymbolMetrics;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.nio.file.Files;
import java.nio.file.Paths;



public class StatisticsUtil {

    public static double getMean(List<Double> values) {
        double mean = 0.0;
        if ((values != null) && (values.size() > 0)) {
            for (double value : values) {
                mean += value;
            }
            mean /= values.size();
        }
        return mean;
    }

    public static double getStandardDeviation(List<Double> values) {
        double deviation = 0.0;
        if ((values != null) && (values.size() > 1)) {
            double mean = getMean(values);
            for (double value : values) {
                double delta = value-mean;
                deviation += delta*delta;
            }
            deviation = Math.sqrt(deviation/values.size());
        }
        return deviation;
    }

    public static double getMedian(List<Double> values) {
        double median = 0.0;
        if (values != null) {
            int numValues = values.size();
            if (numValues > 0) {
                Collections.sort(values);
                if ((numValues%2) == 0) {
                    median = (values.get((numValues/2)-1)+values.get(numValues/2))/2.0;
                } else {
                    median = values.get(numValues/2);
                }
            }
        }
        return median;
    }

    public static double getMean(double [] values) {
        double mean = 0.0;
        if ((values != null) && (values.length > 0)) {
            for (double value : values) {
                mean += value;
            }
            mean /= values.length;
        }
        return mean;
    }

    public static double getStandardDeviation(double [] values) {
        double deviation = 0.0;
        if ((values != null) && (values.length > 1)) {
            double mean = getMean(values);
            for (double value : values) {
                double delta = value-mean;
                deviation += delta*delta;
            }
            deviation = Math.sqrt(deviation/values.length);
        }
        return deviation;
    }

    public static double getMedian(double [] values) {
        double median = 0.0;
        if (values != null) {
            int numValues = values.length;
            if (numValues > 0) {
                Arrays.sort(values);
                if ((numValues%2) == 0) {
                    median = (values[(numValues/2)-1]+values[numValues/2])/2.0;
                } else {
                    median = values[numValues/2];
                }
            }
        }
        return median;
    }

    public static int getMode(List<Integer> array) {
        HashMap<Integer, Integer> map = new HashMap<>();
        int result = array.get(0), max = 1;
        for (int arrayItem : array) {
            if (map.putIfAbsent(arrayItem, 1) != null) {
                int count = map.get(arrayItem) + 1;
                map.put(arrayItem, count);
                if (count > max) {
                    max = count;
                    result = arrayItem;
                }
            }
        }

        return result;
    }


    public static ArrayList<SymbolMetrics> readSymbolMetricsCSV(String dir){
        Resource resource = new ClassPathResource(dir);
        try (
                InputStream in = resource.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                CSVParser csvParser = new CSVParser(br, CSVFormat.DEFAULT
                        .withFirstRecordAsHeader()
                        .withIgnoreHeaderCase()
                        .withTrim());
        ) {
            ArrayList<SymbolMetrics> symbolMetrics = new ArrayList<>();
            for (CSVRecord csvRecord : csvParser) {
                SymbolMetrics symbolMetric = new SymbolMetrics();
                symbolMetric.setSymbol(csvRecord.get(0));
                symbolMetric.setReturns(Double.parseDouble(csvRecord.get("Returns")));
                symbolMetric.setVolatility(Double.parseDouble(csvRecord.get("Volatility")));
                symbolMetric.setAverage(Double.parseDouble(csvRecord.get("Average")));
                symbolMetric.setLabel(Integer.parseInt(csvRecord.get("Label")));
                symbolMetrics.add(symbolMetric);
            }
            return symbolMetrics;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<SymbolMetrics>();
    }
}