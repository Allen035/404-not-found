package com.portfolio.management.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockDao {
    /**
     * 根据参数分页查询Symbol
     * @param company
     * @param symbol
     * @param pageStart
     * @param pageEnd
     * @return
     */
    public List<String> querySymbol(@Param("company") String company, @Param("symbol") String symbol, @Param("pageStart") Integer pageStart, @Param("pageEnd") Integer pageEnd);

    /**
     * 根据条件查总数
     * @param company
     * @param symbol
     * @return
     */
    public Integer querySymbolCount(@Param("company") String company, @Param("symbol") String symbol);

    public String queryCompanyBySymbol(@Param("symbol") String symbol);
}
