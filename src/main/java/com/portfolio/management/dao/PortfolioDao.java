package com.portfolio.management.dao;

import com.portfolio.management.model.Portfolio;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface PortfolioDao {

    List<String> queryWatchSymbol(@Param("userId") String userId);

    List<Map> queryLotsByUserIdAndSymbol(@Param("userId") String userId, @Param("symbol") String symbol);


    @Insert("INSERT INTO portfolio (UserId, Symbol, Shares, `Cost-basis`, Notes) values (#{userId}, #{symbol}, #{shares}, #{cost_Basis}, #{notes})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "_id")
    long insertOne(Portfolio portfolio);

    @Update("UPDATE portfolio SET Notes = #{notes}, `Cost-basis` = #{costBasis}, Shares = #{shares} WHERE `_id` = #{id}")
    int updateOne(Portfolio portfolio);

    @Delete("DELETE FROM portfolio WHERE `_id` = #{id}")
    int deleteOne(Portfolio portfolio);

}
