package com.portfolio.management.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WatchListDao {
    /**
     * 根据参数分页查询Symbol
     * @param company
     * @param symbol
     * @param pageStart
     * @param pageEnd
     * @return
     */
    public List<String> queryWatchSymbol(@Param("userId") String userId, @Param("company") String company, @Param("symbol") String symbol, @Param("pageStart") Integer pageStart, @Param("pageEnd") Integer pageEnd);

    /**
     * 根据条件查总数
     * @param company
     * @param symbol
     * @return
     */
    public Integer queryWatchSymbolCount(@Param("userId") String userId, @Param("company") String company, @Param("symbol") String symbol);

    public List<String> queryWatchedStocks(@Param("userLogin") String userLogin);

    public void addSymbol(@Param("userId") String userId, @Param("symbol") String symbol);

    public void addUserWatchlist(@Param("userId") String userId, @Param("watchlist") String watchlist);

    public void removeSymbol(@Param("userId") String userId, @Param("symbol") String symbol);

    public List<String> queryUserWatchList(@Param("userId") String userId);

    public List<String> queryWatchListSymbols(@Param("watchlist") String watchlist);
}
