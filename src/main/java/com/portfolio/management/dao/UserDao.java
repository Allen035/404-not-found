package com.portfolio.management.dao;

import com.portfolio.management.model.User;

import java.util.List;


public interface UserDao {

    void create(String login, String name, String pswd);

    User getUser(String login);

    List<User> listUsers();

    void delete(String login);

    void update(String login, String name);


}
