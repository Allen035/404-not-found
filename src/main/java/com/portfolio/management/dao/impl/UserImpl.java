package com.portfolio.management.dao.impl;

import com.portfolio.management.dao.impl.mapper.UserMapper;
import com.portfolio.management.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.portfolio.management.dao.UserDao;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

@Repository
public class UserImpl implements UserDao{
    private JdbcTemplate jdbcTemplateObject;

    @Autowired
    public void setJdbcTemplateObject(JdbcTemplate jdbcTemplateObject) {
        this.jdbcTemplateObject = jdbcTemplateObject;
    }

    public void create(String login, String name, String pswd) {
        String SQL = "insert into Users (login, `name`, pswd) values (?, ?, ?)";
        jdbcTemplateObject.update( SQL, login, name, pswd);
    }
    public User getUser(String login) {
        String SQL = "select * from Users where login = ?";
        User User = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{login}, new UserMapper());
        return User;
    }
    public List<User> listUsers() {
        String SQL = "select * from Users";
        List <User> Users = jdbcTemplateObject.query(SQL, new UserMapper());
        return Users;
    }
    public void delete(String login) {
        String SQL = "delete from Users where login = ?";
        jdbcTemplateObject.update(SQL, login);
    }
    public void update(String login, String name){
        String SQL = "update Users set `name` = ? where login = ?;";
        jdbcTemplateObject.update(SQL, name, login);
    }


}
