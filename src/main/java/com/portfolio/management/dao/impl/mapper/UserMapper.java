package com.portfolio.management.dao.impl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.portfolio.management.model.User;
import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<User>{
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User User = new User();
        User.setLogin(rs.getString("LOGIN"));
        User.setName(rs.getString("NAME"));
        User.setPswd(rs.getString("PSWD"));

        return User;
    }
}
