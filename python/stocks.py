import numpy as np
from scipy.cluster.vq import kmeans, vq
import pandas as pd
from math import sqrt
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt
from alpha_vantage.timeseries import TimeSeries
from mpl_toolkits.mplot3d import Axes3D
from collections import OrderedDict



def get_data():
    ts = TimeSeries(key='X8RO597ZV071YDUA', output_format='pandas')
    sp500_url = 'https://en.wikipedia.org/wiki/List_of_S%26P_500_companies'

    # read in the url and scrape ticker data
    data_table = pd.read_html(sp500_url)
    tickers = data_table[0][1:][0].tolist()
    prices_list = []
    print("Downloading data from S&P500...")
    for ticker in tickers:
        try:
            print(">", ticker, flush=True)
            prices, meta_data = ts.get_weekly_adjusted(symbol=ticker)
            prices = pd.DataFrame(prices['4. close'])
            prices.columns = [ticker]
            prices.query("date >= '2017-01-01'", inplace=True)
            prices_list.append(prices)
        except:
            pass
        prices_df = pd.concat(prices_list, axis=1)
    prices_df.sort_index(inplace=True)
    prices_df.to_csv('weekly_from_2017.csv', sep=',', encoding='utf-8')
    return 'weekly_from_2017.csv'


def get_3d_features(csv_file="weekly_from_2017.csv"):
    prices_df = pd.read_csv(csv_file, index_col=0)
    prices_df.fillna(prices_df.mean())

    # Calculate average annual percentage return and volatility over theoretical weekly period
    row_counts = prices_df.shape[0]
    returns = prices_df.pct_change().mean() * row_counts
    returns = pd.DataFrame(returns)
    returns.columns = ['Returns']
    returns['Volatility'] = prices_df.pct_change().std() * sqrt(row_counts)
    returns['Average'] = prices_df.mean()

    # drop the outlier stock from our data
    returns.drop('BF.B', inplace=True)
    returns.drop('XRX', inplace=True)
    returns.drop('NKTR', inplace=True)

    returns.to_csv('returns.csv', sep=',', encoding='utf-8')
    return 'returns.csv'


# format the data as a numpy array to feed into the K-Means algorithm
def elbow_curve(csv_file="returns.csv"):
    returns = pd.read_csv(csv_file, index_col=0)

    # normalize data
    returns = (returns - returns.mean()) / returns.std()
    X = np.asarray(
        [np.asarray(returns['Returns']), np.asarray(returns['Volatility']), np.asarray(returns['Average'])]).T

    distortion = []
    for k in range(2, 20):
        k_means = KMeans(n_clusters=k)
        k_means.fit(X)
        distortion.append(k_means.inertia_)

    fig = plt.figure(figsize=(15, 5))
    plt.plot(range(2, 20), distortion)
    plt.grid(True)
    plt.title('Elbow curve')
    plt.show()


def plot_kmeans(csv_file="returns.csv", clusters=6, save_labels=0):
    returns = pd.read_csv(csv_file, index_col=0)

    # normalize data
    returns = (returns - returns.mean()) / returns.std()
    data = np.asarray(
        [np.asarray(returns['Returns']), np.asarray(returns['Volatility']), np.asarray(returns['Average'])]).T
    # computing K-Means with K = 6 (6 clusters)
    centroids, _ = kmeans(data, clusters)
    # assign each sample to a cluster
    labels, _ = vq(data, centroids)

    colors = ("red", "green", "blue", "yellow", "purple", "pink")
    # Create plot
    fig = plt.figure()
    fig.add_subplot(111)
    ax = fig.gca(projection='3d', facecolor="1.0")

    for data, label in zip(data, labels):
        x, y, z = data
        ax.scatter(x, y, z, alpha=0.8, c=colors[label],label=label, edgecolors='none', s=30)

    plt.title('S&P500 K-means Cluster Normalized')
    ax.set_xlabel('Returns')
    ax.set_ylabel('Volatility')
    ax.set_zlabel('Average Value')

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), loc=2)

    plt.show()

    if save_labels:
        details = [(name, cluster) for name, cluster in zip(returns.index, labels)]

        df = pd.DataFrame(details)
        df.sort_values(1, inplace=True)
        df.to_csv('labels.csv', sep=',', encoding='utf-8', header=False, index=False)


def save_labels(csv_file="returns.csv", clusters=6):
    returns = pd.read_csv(csv_file, index_col=0)

    # normalize data
    returns = (returns - returns.mean()) / returns.std()
    data = np.asarray(
        [np.asarray(returns['Returns']), np.asarray(returns['Volatility']), np.asarray(returns['Average'])]).T
    # computing K-Means with K = 6 (6 clusters)
    centroids, _ = kmeans(data, clusters)
    # assign each sample to a cluster
    labels, _ = vq(data, centroids)

    details = [(name, cluster) for name, cluster in zip(returns.index, labels)]

    df = pd.DataFrame(details)
    df.sort_values(1, inplace=True)
    df.to_csv('labels.csv', sep=',', encoding='utf-8', header=False, index=False)
    return 'labels.csv'

