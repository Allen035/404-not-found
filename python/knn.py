from sklearn.neighbors import KNeighborsClassifier
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from collections import OrderedDict


def save_training_data(returns_csv="returns.csv", labels_csv="labels.csv"):
    returns = pd.read_csv(returns_csv, index_col=0)
    labels = pd.read_csv(labels_csv, index_col=0, header=None, names=['Label'])
    df = pd.concat([returns, labels], axis=1)
    df.to_csv('training_data.csv', sep=',', encoding='utf-8')
    return 'training_data.csv'

def plot_training_data(training_file="training_data.csv"):
    train = pd.read_csv(training_file, index_col=0)
    data = train.iloc[:, [0, 1, 2]]
    labels = train.iloc[:, [3]]
    # Create plot
    colors = ("red", "green", "blue", "yellow", "purple", "pink")
    data = np.asarray(
        [np.asarray(data['Returns']), np.asarray(data['Volatility']), np.asarray(data['Average'])]).T
    labels = np.asarray(labels, int)

    fig = plt.figure()
    fig.add_subplot(111)
    ax = fig.gca(projection='3d', facecolor="1.0")

    for data, label in zip(data, labels):
        x, y, z = data
        ax.scatter(x, y, z, alpha=0.8, c=colors[label[0]], label=label[0], edgecolors='none', s=30)
    plt.title('Trained Data')
    ax.set_xlabel('Returns')
    ax.set_ylabel('Volatility')
    ax.set_zlabel('Average Value')

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), loc=2)

    plt.show()


def knn_classifier(predict_X, training_file="training_data.csv",  plotting=1):
    train = pd.read_csv(training_file, index_col=0)
    data = train.iloc[:,[0,1,2]]
    labels = train.iloc[:,[3]]

    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(data, labels)
    predicted_label = knn.predict(predict_X)[0]
    if plotting:
        # Create plot
        colors = ("red", "green", "blue", "yellow", "purple", "pink")
        data = np.asarray(
            [np.asarray(data['Returns']), np.asarray(data['Volatility']), np.asarray(data['Average'])]).T
        labels = np.asarray(labels, int)

        fig = plt.figure()
        fig.add_subplot(111)
        ax = fig.gca(projection='3d', facecolor="1.0")

        for data, label in zip(data, labels):
            x, y, z = data
            ax.scatter(x, y, z, alpha=0.8, c=colors[label[0]], label=label[0], edgecolors='none', s=30)
        # predict_X as black point
        ax.scatter(predict_X[0][0], predict_X[0][1], predict_X[0][2], alpha=1, c="black", label=('predicted '+str(predicted_label)), edgecolors='none', s=100)


        plt.title('KNN Prediction')
        ax.set_xlabel('Returns')
        ax.set_ylabel('Volatility')
        ax.set_zlabel('Average Value')

        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys(),loc=2)

        plt.show()

    return predicted_label



